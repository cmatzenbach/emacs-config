;; ======== SANE DEFAULTS ========
(setq delete-old-versions -1 )    ; delete excess backup versions silently
(setq version-control t )         ; use version control
(setq vc-make-backup-files t )    ; make backups file even when in version controlled dir
(setq backup-directory-alist `(("." . "~/.emacs.d/backups")) ) ; which directory to put backups file
(setq vc-follow-symlinks t )               ; don't ask for confirmation when opening symlinked file
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)) ) ;transform backups file name
(setq inhibit-startup-screen t )  ; inhibit useless and old-school startup screen
(setq ring-bell-function 'ignore )  ; silent bell when you make a mistake
(setq coding-system-for-read 'utf-8 ) ; use utf-8 by default
(setq coding-system-for-write 'utf-8 )
(setq sentence-end-double-space nil)  ; sentence SHOULD end with only a point.
(setq fill-column 80)   ; toggle wrapping text at the 80th character
(menu-bar-mode -1)                      ; disable menu bar
(setq initial-scratch-message "Welcome to Emacs") ; print a default message in the empty scratch buffer opened at startup
(setq user-full-name "Chris Matzenbach")
(setq user-mail-address "matzy@protonmail.com")
;; (global-linum-mode 1)
;; (scroll-bar-mode -1)
(setq tool-bar-mode -1)
;; (setq-default indent-tabs-mode t)     ; use spaces instead of tabs
(setq-default tab-width 4)
;; avoid having to answer "yes" and "no" every time - change to "y" and "n"
(defalias 'yes-or-no-p 'y-or-n-p)
;; keep fringes clean
(setq-default indicate-empty-lines nil)
;; no more ugly line splitting
(setq-default truncate-lines t)
;; maximum color in major mode syntax highlighting
(setq font-lock-maximum-decoration t)
;; add padding to top of window - need to change its bg color
;; (setq header-line-format "")

;; For MacOS - Add these to the PATH so that proper executables are found
(setenv "PATH" (concat (getenv "PATH") ":/usr/texbin"))
(setenv "PATH" (concat (getenv "PATH") ":/usr/bin"))
(setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
(setenv "PATH" (concat (getenv "PATH") ":/Users/matzy/.npm-global/bin"))
(setq exec-path (append exec-path '("/usr/texbin")))
(setq exec-path (append exec-path '("/usr/bin")))
(setq exec-path (append exec-path '("/usr/local/bin")))
(setq exec-path (append exec-path '("/Users/matzy/.npm-global/bin")))


;; ======== INDENTATION SETTINGS ========
;; MERGE: further testing needed with new indentation detection system
;; (setq-default indent-tabs-mode nil)     ; use spaces instead of tabs
;; (setq-default tab-width 4)
;; set indentation per mode
;; (setq-default tab-width 4
;; 			  c-basic-offset 4
;; 			  coffee-tab-width 4
;; 			  javascript-2-level 4
;; 			  js-2-level 4
;; 			  js2-basic-offset 4
;; 			  web-mode-markup-2-offset 4
;; 			  web-mode-css-2-offset 4
;; 			  web-mode-code-2-offset 4
;; 			  css-2-offset 4
;; 			  rust-indent-offset 4)

;; detect if project is using spaces or tabs
(defun infer-indentation-style ()
  ;; if our source file uses tabs, we use tabs, if spaces spaces, and if        
  ;; neither, we use the current indent-tabs-mode                               
  (let ((space-count (how-many "^  " (point-min) (point-max)))
        (tab-count (how-many "^\t" (point-min) (point-max))))
    (if (> space-count tab-count) (setq indent-tabs-mode nil))
    (if (> tab-count space-count) (setq indent-tabs-mode t))))


;; ======== HISTORY ========
(setq savehist-file "~/.emacs.d/savehist")
(savehist-mode 1)
(setq history-length t)
(setq history-delete-duplicates t)
(setq savehist-save-minibuffer-history 1)
(setq savehist-additional-variables
    '(kill-ring
    search-ring
    regexp-search-ring))


;; ======== PACKAGE SETUP/USE-PACKAGE ========
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("melpa". "http://melpa.org/packages/")
						 ("gnu" . "http://elpa.gnu.org/packages/")
						 ("org" . "https://orgmode.org/elpa/")
						 ("marmalade" . "https://marmalade-repo.org/packages/")
						 ("melpa-stable" . "https://melpa-stable.milkbox.net/packages/")))
(package-initialize)

;; bootstrap 'use-package'
(unless (package-installed-p 'use-package) ; unless it is already installed
  (package-refresh-contents) ; update package archives
  (package-install 'use-package)) ; install most recent version of use-package
;; highlight function symbols in function calls
(use-package highlight-function-calls
  :config
  (add-hook 'emacs-lisp-mode-hook 'highlight-function-calls-mode))
;; activate highlight-escape-sequences mode (locally installed package)
(require 'use-package)
;; always download packages if not already installed
(setq use-package-always-ensure t)
;; TESTING enable imenu support for use-package
;; (setq use-package-enable-imenu-support t)

;; quelpa - builds packages from any source you want
(use-package quelpa)
(use-package quelpa-use-package)

;; LOCAL - get nvm path loaded for bins
(setq exec-path (append exec-path '("/home/matzy/.nvm/versions/node/v6.14.4/bin")))


;; ======== LOCAL CONFIG FILES ========
(defconst user-config-dir "~/.emacs.d/config/")

(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file user-config-dir)))

;; load custom config modules
(load-user-file "appearance.el")
;; load manually installed local packages
(add-to-list 'load-path "~/.emacs.d/local-packages/")
;; (add-to-list 'load-path "~/.emacs.d/config/")
(load "let-alist-1.0.5.el")
(load "highlight-escape-sequences.el")
(load "highlight-quoted.el")
;; (load "brin-theme.el")
;; (load "evil-evilified-state.el")
(load "brin-theme-mod.el")


;; ======== GLOBAL SETTINGS ========
;; additional syntax highlighting
;; highlights elisp symbols
(use-package highlight-defined)
;; highlight numbers and operators
(use-package highlight-numbers
  :hook (prog-mode-hook . highlight-numbers-mode))
(use-package highlight-operators
  :hook (prog-mode-hook . highlight-operator-mode))
;; highlight local code block
(use-package highlight-blocks)
;; highlight doxygen comment blocks
(use-package highlight-doxygen
  :hook (prog-mode-hook . highlight-doxygen-mode))
;; highlights currently indent level
(use-package highlight-indent-guides
  :hook (prog-mode-hook . highlight-indent-guides-mode)
  :config (setq highlight-indent-guides-method 'character))
;; highlight escape sequences in strings
(use-package highlight-escape-sequences)
;; highlight function symbols in function calls
(use-package highlight-function-calls
  :config
  (add-hook 'emacs-lisp-mode-hook 'highlight-function-calls-mode))
;; activate highlight-escape-sequences mode (locally installed package)
(hes-mode)
(add-hook 'prog-mode-hook 'highlight-numbers-mode)
;; if you want 4 space width tabs
(add-hook 'prog-mode-hook '(lambda ()
               (setq c-basic-offset 4)
               (setq indent-tabs-mode t)
               (setq tab-width 4)))

;; additional font-locks
(use-package morlock
  :config (global-morlock-mode 1))
(use-package lisp-extra-font-lock
  :config (lisp-extra-font-lock-global-mode 1))
(use-package cl-lib-highlight
  :config (cl-lib-highlight-initialize))


;; ======== COUNSEL/IVY/SWIPER ========
(use-package counsel
  :diminish ivy-mode
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t
		ivy-count-format "%d/%d ")

  (setq ivy-re-builders-alist
		'((t . ivy--regex-ignore-order)))
  :bind
  ("C-s" . swiper)
  ;; https://www.reddit.com/r/emacs/comments/6i8rmb/noob_change_ctrlnp_to_vimlike_binding_for_ivy_and/
  (:map ivy-minibuffer-map
		("C-h" . evil-delete-char) ; supposed to be (kbd "DEL")
		("C-k" . ivy-previous-line)
		("C-j" . ivy-next-line)
		("C-l" . ivy-alt-done)))

(use-package ivy-hydra
  :after ivy-mode)


;; ======== EVIL MODE ========
(use-package evil
  :diminish evil-mode
  :init
  ;; mERGE - may cause problems - may need to load after all evil related configs are loaded
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  :config
  (evil-mode 1))

;; evil-collection - WIP package to create evil keybindings for missing modes
(require 'evil-collection-minibuffer)
(use-package evil-collection
  :after evil
  :custom (evil-collection-minibuffer-setup)
  :config (evil-collection-init))

;; re-bind universal-argument from C-u to C-f
(define-key global-map (kbd "C-y") 'universal-argument)
(define-key universal-argument-map (kbd "C-u") nil)
(define-key universal-argument-map (kbd "C-y") 'universal-argument-more)
(define-key global-map (kbd "C-u") 'kill-whole-line)
(eval-after-load 'evil-maps
  '(progn
	 (define-key evil-motion-state-map (kbd "C-y") nil)
	 (define-key evil-motion-state-map (kbd "C-u") 'evil-scroll-up)))

;; map fd to escape normal mode
(use-package key-chord)
(key-chord-mode 1)
(key-chord-define-global "fd" 'evil-normal-state)

;; ;; use esc to exit minibuffer
;; (defun minibuffer-keyboard-quit ()
;;   "Abort recursive edit.
;;   In Delete Selection mode, if the mark is active, just deactivate it;
;;   then it takes a second \\[keyboard-quit] to abort the minibuffer."
;;   (interactive)
;;   (if (and delete-selection-mode transient-mark-mode mark-active)
;;       (setq deactivate-mark  t)
;;     (when (get-buffer "*Completions*") (delete-windows-on "*Completions*"))
;;     (abort-recursive-edit))) 

;; ;; make esc get me out of different situations
;; (define-key evil-normal-state-map [escape] 'keyboard-quit)
;; (define-key evil-visual-state-map [escape] 'keyboard-quit)
;; (define-key minibuffer-local-map [escape] 'minibuffer-keyboard-quit)
;; (define-key minibuffer-local-ns-map [escape] 'minibuffer-keyboard-quit)
;; (define-key minibuffer-local-completion-map [escape] 'minibuffer-keyboard-quit)
;; (define-key minibuffer-local-must-match-map [escape] 'minibuffer-keyboard-quit)
;; (define-key minibuffer-local-isearch-map [escape] 'minibuffer-keyboard-quit)
                    ;(global-set-key [escape] 'evil-exit-emacs-state)
;; enabless evil-surrious, which allows you to use ci( or ca{ to change what's either inside or outside enclosing character
(use-package evil-surround
  :config
  (global-evil-surround-mode 1))


;; set c-u to have vim-like behavior (scroll up half page)
(define-key evil-normal-state-map (kbd "C-u") 'evil-scroll-up)
(define-key evil-visual-state-map (kbd "C-u") 'evil-scroll-up)
(define-key evil-insert-state-map (kbd "C-u")
  (lambda ()
  (interactive)
  (evil-delete (point-at-bol) (point))))


(defun lunaryorn-use-js-executables-from-node-modules ()
  "Set executables of JS checkers from local node modules."
  (-when-let* ((file-name (buffer-file-name))
			   (root (locate-dominating-file file-name "node_modules"))
			   (module-directory (expand-file-name "node_modules" root)))
	(pcase-dolist (`(,checker . ,module) '((javascript-jshint . "jshint")
										   (javascript-eslint . "eslint")
										   (javascript-jscs   . "jscs")))
	  (let ((package-directory (expand-file-name module module-directory))
			(executable-var (flycheck-checker-executable-variable checker)))
		(when (file-directory-p package-directory)
		  (set (make-local-variable executable-var)
			   (expand-file-name (concat "bin/" module ".js")
								 package-directory)))))))



;; ======== INDENTATION/BRACKET MANAGEMENT ========
;; evil-smartparens
(use-package smartparens)
(require 'smartparens-config)
(show-smartparens-global-mode +1)
(define-key smartparens-mode-map (kbd ")") #'sp-paredit-like-close-round)
(use-package evil-smartparens :defer t)

;; evil-cleverparens
(use-package evil-cleverparens :defer t)
;; load evil-cleverparens optional text-objects
(require 'evil-cleverparens-text-objects)
;; remap ] and } from evil-cp to sensible bindings

; 
;    ("["   . evil-cp-previous-opening)
;    ("]"   . evil-cp-next-closing)
;    ("]"   . evil-cp-next-closing)
;    ("]"   . evil-cp-next-closing)
;    ("{"   . evil-cp-next-opening)
;    ("}"   . evil-cp-previous-closing)
(evil-define-key '(normal visual) evil-cleverparens-mode-map
  "]" 'evil-cp-previous-closing
  "}" 'evil-cp-next-closing)

;; hydra for smartparens
(defhydra ptrv/smartparens (:hint nil)
  "
Sexps (quit with _q_)
^Nav^            ^Barf/Slurp^              ^Depth^
^---^------------^----------^--------------^-----^-----------------
_f_: forward     _>_:       slurp forward   _R_: splice
_b_: backward    _C->_:     barf forward    _r_: raise
_h_: backward ↑  _<_:       slurp backward  _↑_: raise backward
_d_: down        _C-<_:     barf backward   _↓_: raise forward
_p_: backward ↓
_u_: up
^Jump/Kill^      ^Misc^                    ^Wrap^
^----^-----------^----^--------------------^----^------------------
_0_: beginning   _j_: join                 _(_: wrap with ( )
_$_: end         _s_: split                _{_: wrap with { }
_w_: next        _t_: transpose            _'_: wrap with ' '
_z_: previous    _c_: convolute            _\"_: wrap with \" \"
_y_: copy        _i_: indent defun         _m_: unwrap
_k_: kill        
"
  ("q" nil)
  ;; Wrapping
  ("(" (lambda (_) (interactive "P") (sp-wrap-with-pair "(")) :exit t)
  ("{" (lambda (_) (interactive "P") (sp-wrap-with-pair "{")) :exit t)
  ("'" (lambda (_) (interactive "P") (sp-wrap-with-pair "'")) :exit t)
  ("\"" (lambda (_) (interactive "P") (sp-wrap-with-pair "\"")) :exit t)
  ("m" sp-unwrap-sexp :exit t)
  ;; Navigation
  ("f" sp-forward-sexp)
  ("b" sp-backward-sexp)
  ("h" sp-backward-up-sexp)
  ("d" sp-down-sexp)
  ("p" sp-backward-down-sexp)
  ("u" sp-up-sexp)
  ;; Jump/Kill/copy
  ("0" sp-beginning-of-sexp :exit t)
  ("$" sp-end-of-sexp :exit t)
  ("w" sp-next-sexp)
  ("z" sp-previous-sexp)
  ("y" sp-copy-sexp :exit t)
  ("k" sp-kill-sexp :exit t)
  ;; Misc
  ("t" sp-transpose-sexp)
  ("j" sp-join-sexp)
  ("s" sp-split-sexp)
  ("c" sp-convolute-sexp)
  ("i" sp-indent-defun)
  ;; Depth changing
  ("R" sp-splice-sexp)
  ("r" sp-splice-sexp-killing-around)
  ("<up>" sp-splice-sexp-killing-backward)
  ("<down>" sp-splice-sexp-killing-forward)
  ;; Barfing/slurping
  (">" sp-forward-slurp-sexp :exit t)
  ("C->" sp-forward-barf-sexp :exit t)
  ("C-<" sp-backward-barf-sexp :exit t)
  ("<" sp-backward-slurp-sexp :exit t))

;; hydra for learning smartparens
(defhydra hydra-learn-sp (:hint nil)
  "
  _B_ backward-sexp            -----
  _F_ forward-sexp               _s_ splice-sexp
  _L_ backward-down-sexp         _df_ splice-sexp-killing-forward
  _H_ backward-up-sexp           _db_ splice-sexp-killing-backward
^^------                         _da_ splice-sexp-killing-around
  _k_ down-sexp                -----
  _j_ up-sexp                    _C-s_ select-next-thing-exchange
-^^-----                         _C-p_ select-previous-thing
  _n_ next-sexp                  _C-n_ select-next-thing
  _p_ previous-sexp            -----
  _a_ beginning-of-sexp          _C-f_ forward-symbol
  _z_ end-of-sexp                _C-b_ backward-symbol
--^^-                          -----
  _t_ transpose-sexp             _c_ convolute-sexp
-^^--                            _g_ absorb-sexp
  _x_ delete-char                _q_ emit-sexp
  _dw_ kill-word               -----
  _dd_ kill-sexp                 _,b_ extract-before-sexp
-^^--                            _,a_ extract-after-sexp
  _S_ unwrap-sexp              -----
-^^--                            _AP_ add-to-previous-sexp
  _C-h_ forward-slurp-sexp       _AN_ add-to-next-sexp
  _C-l_ forward-barf-sexp      -----
  _C-S-h_ backward-slurp-sexp    _ join-sexp
  _C-S-l_ backward-barf-sexp     _|_ split-sexp
"
  ;; TODO: Use () and [] - + * | <space>
  ("B" sp-backward-sexp );; similiar to VIM b
  ("F" sp-forward-sexp );; similar to VIM f
  ;;
  ("L" sp-backward-down-sexp )
  ("H" sp-backward-up-sexp )
  ;;
  ("k" sp-down-sexp ) ; root - towards the root
  ("j" sp-up-sexp )
  ;;
  ("n" sp-next-sexp )
  ("p" sp-previous-sexp )
  ;; a..z
  ("a" sp-beginning-of-sexp )
  ("z" sp-end-of-sexp )
  ;;
  ("t" sp-transpose-sexp )
  ;;
  ("x" sp-delete-char )
  ("dw" sp-kill-word )
  ;;("ds" sp-kill-symbol ) ;; Prefer kill-sexp
  ("dd" sp-kill-sexp )
  ;;("yy" sp-copy-sexp ) ;; Don't like it. Pref visual selection
  ;;
  ("S" sp-unwrap-sexp ) ;; Strip!
  ;;("wh" sp-backward-unwrap-sexp ) ;; Too similar to above
  ;;
  ("C-h" sp-forward-slurp-sexp )
  ("C-l" sp-forward-barf-sexp )
  ("C-S-h" sp-backward-slurp-sexp )
  ("C-S-l" sp-backward-barf-sexp )
  ;;
  ;;("C-[" (bind (sp-wrap-with-pair "[")) ) ;;FIXME
  ;;("C-(" (bind (sp-wrap-with-pair "(")) )
  ;;
  ("s" sp-splice-sexp )
  ("df" sp-splice-sexp-killing-forward )
  ("db" sp-splice-sexp-killing-backward )
  ("da" sp-splice-sexp-killing-around )
  ;;
  ("C-s" sp-select-next-thing-exchange )
  ("C-p" sp-select-previous-thing )
  ("C-n" sp-select-next-thing )
  ;;
  ("C-f" sp-forward-symbol )
  ("C-b" sp-backward-symbol )
  ;;
  ;;("C-t" sp-prefix-tag-object)
  ;;("H-p" sp-prefix-pair-object)
  ("c" sp-convolute-sexp )
  ("g" sp-absorb-sexp )
  ("q" sp-emit-sexp )
  ;;
  (",b" sp-extract-before-sexp )
  (",a" sp-extract-after-sexp )
  ;;
  ("AP" sp-add-to-previous-sexp );; Difference to slurp?
  ("AN" sp-add-to-next-sexp )
  ;;
  ("_" sp-join-sexp ) ;;Good
  ("|" sp-split-sexp ))


;; make emacs recognize .editorconfig files
(use-package editorconfig
  :config
  (editorconfig-mode 1))

;; rainbow delimiters, a godsend for lisps and shitty es5 callback stacks from hell
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))
;; if i used :hook correctly above the line below can be removed
;; (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

(defhydra ptrv/smartparens (:hint nil)
  "
Sexps (quit with _q_)
^Nav^            ^Barf/Slurp^              ^Depth^
^---^------------^----------^--------------^-----^-----------------
_f_: forward     _>_:       slurp forward   _R_: splice
_b_: backward    _C->_:     barf forward    _r_: raise
_h_: backward ↑  _<_:       slurp backward  _↑_: raise backward
_d_: down        _C-<_:     barf backward   _↓_: raise forward
_p_: backward ↓
_u_: up
^Jump/Kill^      ^Misc^                    ^Wrap^
^----^-----------^----^--------------------^----^------------------
_0_: beginning   _j_: join                 _(_: wrap with ( )
_$_: end         _s_: split                _{_: wrap with { }
_w_: next        _t_: transpose            _'_: wrap with ' '
_z_: previous    _c_: convolute            _\"_: wrap with \" \"
_y_: copy        _i_: indent defun         _m_: unwrap
_k_: kill        
"
  ("q" nil)
  ;; Wrapping
  ("(" (lambda (_) (interactive "P") (sp-wrap-with-pair "(")) :exit t)
  ("{" (lambda (_) (interactive "P") (sp-wrap-with-pair "{")) :exit t)
  ("'" (lambda (_) (interactive "P") (sp-wrap-with-pair "'")) :exit t)
  ("\"" (lambda (_) (interactive "P") (sp-wrap-with-pair "\"")) :exit t)
  ("m" sp-unwrap-sexp :exit t)
  ;; Navigation
  ("f" sp-forward-sexp)
  ("b" sp-backward-sexp)
  ("h" sp-backward-up-sexp)
  ("d" sp-down-sexp)
  ("p" sp-backward-down-sexp)
  ("u" sp-up-sexp)
  ;; Jump/Kill/copy
  ("0" sp-beginning-of-sexp :exit t)
  ("$" sp-end-of-sexp :exit t)
  ("w" sp-next-sexp)
  ("z" sp-previous-sexp)
  ("y" sp-copy-sexp :exit t)
  ("k" sp-kill-sexp :exit t)
  ;; Misc
  ("t" sp-transpose-sexp)
  ("j" sp-join-sexp)
  ("s" sp-split-sexp)
  ("c" sp-convolute-sexp)
  ("i" sp-indent-defun)
  ;; Depth changing
  ("R" sp-splice-sexp)
  ("r" sp-splice-sexp-killing-around)
  ("<up>" sp-splice-sexp-killing-backward)
  ("<down>" sp-splice-sexp-killing-forward)
  ;; Barfing/slurping
  (">" sp-forward-slurp-sexp :exit t)
  ("C->" sp-forward-barf-sexp :exit t)
  ("C-<" sp-backward-barf-sexp :exit t)
  ("<" sp-backward-slurp-sexp :exit t))

;; hydra for learning smartparens
(defhydra hydra-learn-sp (:hint nil)
  "
  _B_ backward-sexp            -----
  _F_ forward-sexp               _s_ splice-sexp
  _L_ backward-down-sexp         _df_ splice-sexp-killing-forward
  _H_ backward-up-sexp           _db_ splice-sexp-killing-backward
^^------                         _da_ splice-sexp-killing-around
  _k_ down-sexp                -----
  _j_ up-sexp                    _C-s_ select-next-thing-exchange
-^^-----                         _C-p_ select-previous-thing
  _n_ next-sexp                  _C-n_ select-next-thing
  _p_ previous-sexp            -----
  _a_ beginning-of-sexp          _C-f_ forward-symbol
  _z_ end-of-sexp                _C-b_ backward-symbol
--^^-                          -----
  _t_ transpose-sexp             _c_ convolute-sexp
-^^--                            _g_ absorb-sexp
  _x_ delete-char                _q_ emit-sexp
  _dw_ kill-word               -----
  _dd_ kill-sexp                 _,b_ extract-before-sexp
-^^--                            _,a_ extract-after-sexp
  _S_ unwrap-sexp              -----
-^^--                            _AP_ add-to-previous-sexp
  _C-h_ forward-slurp-sexp       _AN_ add-to-next-sexp
  _C-l_ forward-barf-sexp      -----
  _C-S-h_ backward-slurp-sexp    _ join-sexp
  _C-S-l_ backward-barf-sexp     _|_ split-sexp
"
  ;; TODO: Use () and [] - + * | <space>
  ("B" sp-backward-sexp );; similiar to VIM b
  ("F" sp-forward-sexp );; similar to VIM f
  ;;
  ("L" sp-backward-down-sexp )
  ("H" sp-backward-up-sexp )
  ;;
  ("k" sp-down-sexp ) ; root - towards the root
  ("j" sp-up-sexp )
  ;;
  ("n" sp-next-sexp )
  ("p" sp-previous-sexp )
  ;; a..z
  ("a" sp-beginning-of-sexp )
  ("z" sp-end-of-sexp )
  ;;
  ("t" sp-transpose-sexp )
  ;;
  ("x" sp-delete-char )
  ("dw" sp-kill-word )
  ;;("ds" sp-kill-symbol ) ;; Prefer kill-sexp
  ("dd" sp-kill-sexp )
  ;;("yy" sp-copy-sexp ) ;; Don't like it. Pref visual selection
  ;;
  ("S" sp-unwrap-sexp ) ;; Strip!
  ;;("wh" sp-backward-unwrap-sexp ) ;; Too similar to above
  ;;
  ("C-h" sp-forward-slurp-sexp )
  ("C-l" sp-forward-barf-sexp )
  ("C-S-h" sp-backward-slurp-sexp )
  ("C-S-l" sp-backward-barf-sexp )
  ;;
  ;;("C-[" (bind (sp-wrap-with-pair "[")) ) ;;FIXME
  ;;("C-(" (bind (sp-wrap-with-pair "(")) )
  ;;
  ("s" sp-splice-sexp )
  ("df" sp-splice-sexp-killing-forward )
  ("db" sp-splice-sexp-killing-backward )
  ("da" sp-splice-sexp-killing-around )
  ;;
  ("C-s" sp-select-next-thing-exchange )
  ("C-p" sp-select-previous-thing )
  ("C-n" sp-select-next-thing )
  ;;
  ("C-f" sp-forward-symbol )
  ("C-b" sp-backward-symbol )
  ;;
  ;;("C-t" sp-prefix-tag-object)
  ;;("H-p" sp-prefix-pair-object)
  ("c" sp-convolute-sexp )
  ("g" sp-absorb-sexp )
  ("q" sp-emit-sexp )
  ;;
  (",b" sp-extract-before-sexp )
  (",a" sp-extract-after-sexp )
  ;;
  ("AP" sp-add-to-previous-sexp );; Difference to slurp?
  ("AN" sp-add-to-next-sexp )
  ;;
  ("_" sp-join-sexp ) ;;Good
  ("|" sp-split-sexp ))


;; ======== SUBLIMITY ========
(use-package sublimity)
;; smooth scrolling
(require 'sublimity-scroll)
(sublimity-mode 1)


;; ======== PERSPECTIVE ========
;; (use-package persp-mode)
;; (persp-mode)


;; ======== EYEBROWSE ========
(use-package eyebrowse
  :init
  (eyebrowse-mode))


;; ======== ACE-WINDOW ========
(use-package ace-window)

;; window hydra
(defhydra hydra-window ()
  "
Movement^^        ^Split^         ^Switch^    ^Resize^
----------------------------------------------------------------
_h_ ←         _v_ertical      _b_uffer        _q_ X←
_j_ ↓         _x_ horizontal  _f_ind files    _w_ X↓
_k_ ↑         _z_ undo        _a_ce 1         _e_ X↑
_l_ →         _Z_ reset       _s_wap          _r_ X→
_F_ollow    _D_lt Other       _S_ave          max_i_mize
_SPC_ cancel  _o_nly this     _d_elete  
"
  ("h" windmove-left )
  ("j" windmove-down )
  ("k" windmove-up )
  ("l" windmove-right )
  ("q" hydra-move-splitter-left)
  ("w" hydra-move-splitter-down)
  ("e" hydra-move-splitter-up)
  ("r" hydra-move-splitter-right)
  ("b" ivy-switch-buffer)
  ("f" counsel-find-files)
  ("F" follow-mode)
  ("a" (lambda ()
     (interactive)
     (ace-window 1)
     (add-hook 'ace-window-end-once-hook
           'hydra-window/body))
   )
  ("v" (lambda ()
     (interactive)
     (split-window-right)
     (windmove-right))
   )
  ("x" (lambda ()
     (interactive)
     (split-window-below)
     (windmove-down))
   )
  ("s" (lambda ()
     (interactive)
     (ace-window 4)
     (add-hook 'ace-window-end-once-hook
           'hydra-window/body)))
  ("S" save-buffer)
  ("d" delete-window)
  ("D" (lambda ()
     (interactive)
     (ace-window 16)
     (add-hook 'ace-window-end-once-hook
           'hydra-window/body))
   )
  ("o" delete-other-windows)
  ("i" ace-delete-other-windows)
  ("z" (progn
     (winner-undo)
     (setq this-command 'winner-undo))
   )
  ("Z" winner-redo)
  ("SPC" nil)
  )


;; ======== AVY ========
(use-package avy
  :config
  (avy-setup-default)
  (defhydra hydra-avy (global-map "M-g" :color blue)
  "avy-goto"
  ("c" avy-goto-char "char")
  ("C" avy-goto-char-2 "char-2")
  ("w" avy-goto-word-1 "word")
  ("s" avy-goto-subword-1 "subword")
  ("u" link-hint-open-link "open-URI")
  ("U" link-hint-copy-link "copy-URI"))
  )

;; ======== CORRAL ========
(use-package corral
  :config
  (defhydra hydra-corral (:columns 4)
	"Corral"
	("(" corral-parentheses-backward "Back")
	(")" corral-parentheses-forward "Forward")
	("[" corral-brackets-backward "Back")
	("]" corral-brackets-forward "Forward")
	("{" corral-braces-backward "Back")
	("}" corral-braces-forward "Forward")
	("." hydra-repeat "Repeat"))
  (global-set-key (kbd "C-c c") #'hydra-corral/body))

;; ======== DIRED ========
;; (use-package dired
;;   :bind (("C-x C-d" . dired)
;; 		 ("C-x C-j" . dired-jump)
;; 		 ("C-x 4 j" . dired-jump-other-window))
;;   :config
;;   (require 'dired-x)
;;   (defhydra hydra-dired (:color pink :columns 3 :hint nil)
;; "
;; ^Mark^‗‗‗‗‗‗‗^Flag^‗‗‗‗‗‗‗‗^Emacs Op^‗‗‗‗‗‗^‗^‗‗‗‗‗‗‗‗‗‗‗‗‗^^File Op^^‗‗(_e_dit)
;; _*_: marks^^   _#_: temp^^     _Q_uery/rep     _F_ind marked   _!_shell_&_ _S_ymlink
;; _%_: regexp^^  _~_: backup^^   _A_: grep       _L_oad          ^^_C_opy    _H_ardlink
;; _u_n/_m_ark    _d_: this^^     _B_yte compile  _k_ill line     ^^_D_elete  ch_M_od
;; _t_oggle/_U_   _z_ap^^         _v_iew          _w_: file name  ^^_R_ename  ch_O_wn
;; _[_ _]_:page   _<_ _>_:dirline _o_ther win     redisp_l_ay     ^^_T_ouch   ch_G_rp
;; "
;; 	("SPC" nil)
;; 	("RET" dired-find-file :exit t)
;; 	("q" quit-window :exit t)
;; 	("e" dired-toggle-read-only)
;; 	("!" dired-do-shell-command)
;; 	("m" dired-mark)
;; 	("u" dired-unmark)
;; 	("#" dired-flag-auto-save-files)
;; 	("$" dired-hide-subdir "hide subdir")
;; 	("%" hydra-dired-regexp/body :exit t)
;; 	("&" dired-do-async-shell-command)
;; 	("(" dired-hide-details-mode "hide details")
;; 	("*" hydra-dired-mark/body :exit t)
;; 	("+" dired-create-directory "create dir")
;; 	("." dired-clean-directory "clean dir")
;; 	("<" dired-prev-dirline)
;; 	("=" dired-diff "diff")
;; 	(">" dired-next-dirline)
;; 	("[" backward-page)
;; 	("]" forward-page)
;; 	("A" dired-do-find-regexp)
;; 	("B" dired-do-byte-compile)
;; 	("C" dired-do-copy)
;; 	("D" dired-do-delete)
;; 	("F" dired-do-find-marked-files :exit t)
;; 	("G" dired-do-chgrp)
;; 	("H" dired-do-hardlink)
;; 	("L" dired-do-load)
;; 	("M" dired-do-chmod)
;; 	("O" dired-do-chown)
;; 	("P" dired-do-print "print")
;; 	("Q" dired-do-find-regexp-and-replace)
;; 	("R" dired-do-rename)
;; 	("S" dired-do-symlink)
;; 	("T" dired-do-touch)
;; 	("U" dired-unmark-all-marks)
;; 	("W" browse-url-of-dired-file "Web")
;; 	("Z" dired-do-compress "compress")
;; 	("^" dired-up-directory "up-directory")
;; 	("a" dired-find-alternate-file "find-alternate-file")
;; 	("c" dired-do-compress-to "compress-to")
;; 	("d" dired-flag-file-deletion)
;; 	("i" dired-maybe-insert-subdir "maybe-insert-subdir")
;; 	("j" dired-goto-file "goto-file")
;; 	("k" dired-do-kill-lines)
;; 	("l" dired-do-redisplay)
;; 	("o" dired-find-file-other-window :exit t)
;; 	("s" dired-sort-toggle-or-edit "sort-toggle-or-edit")
;; 	("t" dired-toggle-marks)
;; 	("v" dired-view-file :exit t)
;; 	("w" dired-copy-filename-as-kill)
;; 	("z" dired-do-flagged-delete)
;; 	("y" dired-show-file-type "show-file-type")
;; 	("~" dired-flag-backup-files))

;;   (defhydra hydra-dired-mark (:color teal :columns 3 :hi						 :after-exit
;; 									 (if (eq major-mode 'dired-mode)
;; 										 (hydra-dired/body)))
;; 	"Mark"
;; 	("SPC" nil)
;; 	("!" dired-unmark-all-marks  "unmark all")
;; 	("%" dired-mark-files-regexp "regexp")
;; 	("(" dired-mark-sexp         "sexp")
;; 	("*" dired-mark-executables  "executables")
;; 	("." dired-mark-extension    "extension")
;; 	("/" dired-mark-directories  "directories")
;; 	("?" dired-unmark-all-files  "unmark markchar")
;; 	("@" dired-mark-symlinks     "symlinks")
;; 	("O" dired-mark-omitted      "omitted")
;; 	("c" dired-change-marks      "change")
;; 	("s" dired-mark-subdir-files "subdir-files"))

;;   (defhydra hydra-dired-regexp (:color teal :columns 3 :hint nil
;; 				       :after-exit
;; 				       (if (eq major-mode 'dired-mode)					   (hydra-dired/body)))
;;     "Regexp"
;;     ("SPC" nil)
;;     ("&" dired-flag-garbage-files "flag-garbage-files")
;;     ("C" dired-do-copy-regexp "copy")
;;     ("H" dired-do-hardlink-regexp "hardlink")
;;     ("R" dired-do-rename-regexp "rename")
;;     ("S" dired-do-symlink-regexp "symlink")
;;     ("Y" dired-do-relsymlink-regexp "relsymlink")
;;     ("d" dired-flag-files-regexp "flag-files")
;;     ("g" dired-mark-files-containing-regexp "mark-containing")
;;     ("l" dired-downcase "downcase")
;;     ("m" dired-mark-files-regexp "mark")
;;     ("r" dired-do-rename-regexp "rename")
;;     ("u" dired-upcase "upcase"))

;;   (bind-keys :map dired-mode-map
;; 			 ("[" . backward-page)
;; 			 ("]" . forward-page)
;; 			 ("z" . dired-do-flagged-delete)
;; 			 ("x" . god-mode-self-insert)
;; 			 ("e" . dired-toggle-read-only)
;; 			 ("SPC" . hydra-dired/body))
;;   :ensure nil)


;; ======== TARGETS.EL ========
;; TBI: I tried it when it was early in development and did not work well enough.  One of the vim features I miss the most.  Check in on status of repo now?
;; (use-package targets
;;   :load-path "~/.emacs.d/local-packages/targets.el"
;;   :init
;;   (setq targets-user-text-objects '((pipe "|" nil separator)
;;                                     (paren "(" ")" pair :more-keys "b")
;;                                     (bracket "[" "]" pair :more-keys "r")
;;                                     (curly "{" "}" pair :more-keys "c")))
;;   :config
;;   (targets-setup t
;;                  :inside-key "i"
;;                  :around-key "a"
;;                  :remote-key nil))


;; ======== MULTIPLE CURSORS ========
(use-package multiple-cursors)

;; ======== WHICH-KEY && GENERAL ========
(use-package which-key
  :demand t
  :commands (which-key-mode)
  :config
  (progn
	(setq which-key-idle-delay 0.3
		  which-key-allow-evil-operators t))
  (which-key-mode))

(use-package general)

(general-define-key
 :states '(normal visual insert emacs)
 :prefix "SPC"
 :non-normal-prefix "C-SPC"
 "SPC" 'counsel-M-x
 "'" 'new-eshell
 ;:"?" '(eshell- -goto-filedir-or-home :which-key "iterm - goto dir")
 "TAB" 'switch-to-previous-buffer
 "M-x" 'counsel-M-x ;; gives M-x command counsel features
 ";" 'comment-dwim ;; comment out lines

 ;; applications
 "a" '(:ignore t :which-key "Applications")
 "aC" 'copy-buffer-to-clipboard
 "ad" 'dired
 "ai" 'all-the-icons-insert
 "am" 'hydra-macro/body
 "ar" 'start-restclient
 "as" '(:ignore t :which-key "Slack")
 "ass" 'slack-start
 "asgs" '('slack-group-select :which-key "select group")
 "asgr" '('slack-group-list-update :which-key "update group list")
 "asgi" '('slack-group-invite :which-key "invite to group")
 "asis" '('slack-im-select :which-key "select IM")
 "asir" '('slack-im-list-update :which-key "update IM list")
 "asio" '('slack-im-open :which-key "open IM")
 "asrs" '('slack-select-rooms :which-key "select rooms")
 "asrr" '('slack-room-list-update :which-key "update room list")
 "ast" 'slack-message-embed-mention
 "asmd" 'slack-message-delete
 "asme" 'slack-message-edit
 "asmr" 'slack-message-add-reaction
 "asms" 'slack-message-show-reaction-users
 "au" 'counsel-unicode-char

 ;; buffers
 "b" '(:ignore t :which-key "Buffers")
 "bb" 'ivy-switch-buffer
 "bd" 'kill-this-buffer
 "bD" 'kill-buffer-and-window
 "bn" 'next-buffer
 "bp" 'previous-buffer

 ;; multiple cursors
 "c" '(:ignore t :which-key "Multiple Cursors")
 "ce" 'mc/edit-lines
 "cn" 'mc/mark-next-like-this
 "cp" 'mc/mark-previous-like-this
 "ca" 'mc/mark-all-like-this
 "cm" 'mc/mark-more-like-this-extended
 "cN" 'mc/unmark-next-like-this
 "cP" 'mc/unmark-previous-like-this
 "c-<mouse-1>" 'mc/add-cursor-on-click

 ;; elisp!
 "e" '(:ignore t :which-key "Elisp")
 "eb" 'eval-buffer
 "er" 'eval-region

 ;; files
 "f" '(:ignore t :which-key "Files")
 "fa" 'counsel-ag
 "fD" 'delete-current-buffer-file
 "ff" 'counsel-find-file
 "fg" 'counsel-git-grep
 "fl" 'counsel-find-library
 "fr" 'counsel-recentf
 "fed" 'open-config-file
 "feR" 'reload-config-file
 "fg" 'counsel-git-grep
 "fr" 'rename-current-buffer-file
 "fs" 'save-buffer
 "fS" 'save-all-buffers

 ;; git
 "g" '(:ignore t :which-key "Git")
 "gs" 'magit-status

 ;; help
 "h" '(:ignore t :which-key "Help")
 "hc" 'helpful-command
 "hf" 'helpful-callable
 "hk" 'helpful-describe-key
 "hm" 'helpful-macro
 "hp" 'helpful-at-point
 "hv" 'helpful-variable

 ;; ivy/counsel
 "i" '(hydra-ivy/body :which-key "Ivy")

 ;; jump
 "j" '(:ignore t :which-key "Jump")
 "jc" 'avy-goto-char-timer
 "jd" 'move-line-down
 "jfb" 'beginning-of-defun
 "jfe" 'end-of-defun
 "jic" 'counsel-imenu
 "jil" 'imenu-list-minor-mode
 "jl" 'avy-goto-line
 "jn" 'collapse-next-line
 "jq" 'avy-goto-word-0
 "ju" 'move-line-up
 "jw" 'avy-goto-word-1

 ;; perspective
 ;; "l" '(:keymap persp-key-map :package persp-mode :which-key "Layout")
 "l" '(:ignore t :which-key "Layout")
 "l0" 'eyebrowse-switch-to-window-config-0
 "l1" 'eyebrowse-switch-to-window-config-1
 "l2" 'eyebrowse-switch-to-window-config-2
 "l3" 'eyebrowse-switch-to-window-config-3
 "l4" 'eyebrowse-switch-to-window-config-4
 "l5" 'eyebrowse-switch-to-window-config-5
 "l6" 'eyebrowse-switch-to-window-config-6
 "l7" 'eyebrowse-switch-to-window-config-7
 "l8" 'eyebrowse-switch-to-window-config-8
 "l9" 'eyebrowse-switch-to-window-config-9

 "m" 'major-mode-hydra

 ;; org
 "o" '(:ignore t :which-key "Org")
 "oa" 'org-agenda
 "ob" 'org-switchb
 "oc" 'org-capture
 "ol" 'org-store-link

 ;; projectile
 ;; bind p to be the prefix for opening the map of projectile commands
 "p" '(:keymap projectile-command-map :package projectile :which-key "Project")
 
 ;; smartparens
 "s" '(ptrv/smartparens/body :which-key "Smartparens")

 ;; windows
 "w" '(:ignore t :which-key "Windows")
 "wa" 'hydra-window/body
 "wd" 'evil-window-delete
 "wh" 'evil-window-left
 "wj" 'evil-window-down
 "wk" 'evil-window-up
 "wl" 'evil-window-right
 "wr" 'rotate-windows
 "w/" 'evil-window-vsplit
 "w-" 'evil-window-split
 )


;; ======== HYDRA ========
(use-package hydra
  :defer t)

;; major-mode-hydra - hydras for each major mode to replace spacemacs SPC m
(add-to-list 'load-path "~/.emacs.d/local-packages/major-mode-hydra.el")
(require 'major-mode-hydra)
(global-set-key (kbd "C-M-m") 'major-mode-hydra)


;;======== DOOM THEME ========
(use-package doom-themes)
(load-theme 'doom-spacegrey t)

;; Global settings (defaults)
(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
      doom-themes-enable-italic t) ; if nil, italics is universally disabled

;; Enable flashing mode-line on errors
(doom-themes-visual-bell-config)

;; Enable custom neotree theme (all-the-icons must be installed!)
;; (doom-themes-neotree-config)
;; or for treemacs users
;; (doom-themes-treemacs-config)

;; Corrects (and improves) org-mode's native fontification.
(doom-themes-org-config)

(use-package doom-modeline
      :hook (after-init . doom-modeline-init))

;; Customize doom-modeline
(setq doom-modeline-buffer-file-name-style 'truncate-upto-project)
;; (setq doom-modeline-buffer-state-icon nil)


;; ======== COMPANY ========
(use-package company
  :init
  (add-hook 'prog-mode-hook 'global-company-mode)
  :config
  (setq company-tooltip-align-annotations t
    company-minimum-prefix-length 2
    company-idle-delay 0.1))
(setq dabbrev-case-fold-search nil)
(setq company-frontends
    '(company-pseudo-tooltip-frontend
    company-preview-if-just-one-frontend
    company-echo-metadata-frontend))

;; trigger company anytime in insert mode with C-l
(define-key evil-insert-state-map (kbd "C-l") #'company-complete)
;; recenter-top-bottom was originally mapped to C-l, change to C-;
(global-set-key (kbd "C-;") 'recenter-top-bottom)
;; use C-j and C-k to navigate through completion menu, C-l to complete
(with-eval-after-load 'company
  (define-key company-active-map (kbd "M-n") nil)
  (define-key company-active-map (kbd "M-p") nil)
  (define-key company-active-map (kbd "C-j") #'company-select-next)
  (define-key company-active-map (kbd "C-k") #'company-select-previous)
  (define-key company-active-map (kbd "C-l") #'company-complete-selection))

;; helper function to prevent interference between company and yasnippet
(defun company-yasnippet-or-completion ()
  "Locally change the value of yas-fallback-behaviour, which causes yas to call company-complete-common if no completion is found."
  (interactive)
  (let ((yas-fallback-behavior nil))
	(unless (yas-expand)
	  (call-interactively #'company-complete-common))))
;; now we hook this function into company
(add-hook 'company-mode-hook (lambda ()
							   (substitute-key-definition 'company-complete-common
														  'company-yasnippet-or-completion
														  company-active-map)))

;; Company Box (pretty front-end with icons)
(use-package company-box
  :after company
  :hook (company-mode . company-box-mode))

(add-to-list 'load-path "~/.emacs.d/local-packages/")
(require 'font-lock+)
;; pretty icons for company-box (thanks for icons-in-terminal package)
(add-to-list 'load-path "~/.local/share/icons-in-terminal/")
(require 'icons-in-terminal)

;; text icons for company-box
(setq company-box-icons-unknown 'fa_question_circle)

(setq company-box-icons-elisp
    '((fa_tag :face font-lock-function-name-face) ;; Function
    (fa_cog :face font-lock-variable-name-face) ;; Variable
    (fa_cube :face font-lock-constant-face) ;; Feature
    (md_color_lens :face font-lock-doc-face))) ;; Face

(setq company-box-icons-yasnippet 'fa_bookmark)

(setq company-box-icons-acphp
    '((fa_cube :foreground "#7C4DFF") ;; Trait
    (fa_cube :foreground "#7C4DFF") ;; Class
    (fa_tags :face font-lock-function-name-face) ;; Method
    (fa_tag :face font-lock-function-name-face) ;; Function
    (fa_cog :foreground "#FF9800") ;; Property
    (fa_square :face font-lock-constant-face) ;; Constant
    (fa_cog :foreground "#FF9800") ;; Member/Variable
    (fa_cube :foreground "#7C4DFF") ;; Interface
    (fa_cube :face font-lock-type-face) ;; Namespace
    (md_closed_caption :foreground "#009688") ;; Usetrait
    (fa_square_o :face font-lock-constant-face) ;; Fallback
    ))

(setq company-box-icons-lsp
    '((1 . fa_text_height) ;; Text
    (2 . (fa_tags :face font-lock-function-name-face)) ;; Method
    (3 . (fa_tag :face font-lock-function-name-face)) ;; Function
    (4 . (fa_tag :face font-lock-function-name-face)) ;; Constructor
    (5 . (fa_cog :foreground "#FF9800")) ;; Field
    (6 . (fa_cog :foreground "#FF9800")) ;; Variable
    (7 . (fa_cube :foreground "#7C4DFF")) ;; Class
    (8 . (fa_cube :foreground "#7C4DFF")) ;; Interface
    (9 . (fa_cube :foreground "#7C4DFF")) ;; Module
    (10 . (fa_cog :foreground "#FF9800")) ;; Property
    (11 . md_settings_system_daydream) ;; Unit
    (12 . (fa_cog :foreground "#FF9800")) ;; Value
    (13 . (md_storage :face font-lock-type-face)) ;; Enum
    (14 . (md_closed_caption :foreground "#009688")) ;; Keyword
    (15 . md_closed_caption) ;; Snippet
    (16 . (md_color_lens :face font-lock-doc-face)) ;; Color
    (17 . fa_file_text_o) ;; File
    (18 . md_refresh) ;; Reference
    (19 . fa_folder_open) ;; Folder
    (20 . (md_closed_caption :foreground "#009688")) ;; EnumMember
    (21 . (fa_square :face font-lock-constant-face)) ;; Constant
    (22 . (fa_cube :face font-lock-type-face)) ;; Struct
    (23 . fa_calendar) ;; Event
    (24 . fa_square_o) ;; Operator
    (25 . fa_arrows)) ;; TypeParameter
    )

;; Company Quickhelp
;; adds documentation pop-ups to company-mode
(use-package company-quickhelp
  :after company
  :init (setq company-quickhelp-delay 0.5)
  :hook (after-init . company-quickhelp-mode)
  :config
  (define-key company-active-map (kbd "C-c h") #'company-quickhelp-manual-begin)
  (use-package pos-tip
	:ensure t))


;; ======== TREEMACS ========
(use-package treemacs
  :defer t
  :init
  (with-eval-after-load 'winum
	(define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
	(setq treemacs-collapse-dirs                 (if treemacs-python-executable 3 0)
		  treemacs-deferred-git-apply-delay      0.5
		  treemacs-display-in-side-window        t
		  treemacs-eldoc-display                 t
		  treemacs-file-event-delay              5000
		  treemacs-file-follow-delay             0.2
		  treemacs-follow-after-init             t
		  treemacs-git-command-pipe              ""
		  treemacs-goto-tag-strategy             'refetch-index
		  treemacs-indentation                   2
		  treemacs-indentation-string            " "
		  treemacs-is-never-other-window         nil
		  treemacs-max-git-entries               5000
		  treemacs-missing-project-action        'ask
		  treemacs-no-png-images                 nil
		  treemacs-no-delete-other-windows       t
		  treemacs-project-follow-cleanup        nil
		  treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
		  treemacs-position                      'left
		  treemacs-recenter-distance             0.1
		  treemacs-recenter-after-file-follow    nil
		  treemacs-recenter-after-tag-follow     nil
		  treemacs-recenter-after-project-jump   'always
		  treemacs-recenter-after-project-expand 'on-distance
		  treemacs-show-cursor                   nil
		  treemacs-show-hidden-files             t
		  treemacs-silent-filewatch              nil
		  treemacs-silent-refresh                nil
		  treemacs-sorting                       'alphabetic-desc
		  treemacs-space-between-root-nodes      t
		  treemacs-tag-follow-cleanup            t
		  treemacs-tag-follow-delay              1.5
		  treemacs-width                         35)

	;; The default width and height of the icons is 22 pixels. If you are
	;; using a Hi-DPI display, uncomment this to double the icon size.
	;;(treemacs-resize-icons 44)

	(treemacs-follow-mode t)
	(treemacs-filewatch-mode t)
	(treemacs-fringe-indicator-mode t)
	(pcase (cons (not (null (executable-find "git")))
				 (not (null treemacs-python-executable)))
	  (`(t . t)
	   (treemacs-git-mode 'deferred))
	  (`(t . _)
	   (treemacs-git-mode 'simple))))
  :bind
  (:map global-map
		("M-0"       . treemacs-select-window)
		("C-x t 1"   . treemacs-delete-other-windows)
		("C-x t t"   . treemacs)
		("C-x t B"   . treemacs-bookmark)
		("C-x t C-t" . treemacs-find-file)
		("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-evil
  :after treemacs evil)

(use-package treemacs-projectile
  :after treemacs projectile)

(use-package treemacs-icons-dired
  :after treemacs dired
  :config (treemacs-icons-dired-mode))

(use-package treemacs-magit
  :after treemacs magit)


;; ======== DUM JUMP ========
(use-package dumb-jump
  :defer t
  :config
  (setq dumb-jump-selector 'ivy))

(defhydra dumb-jump-hydra (:color blue :columns 3)
  "Dumb Jump"
  ("j" dumb-jump-go "Go")
  ("o" dumb-jump-go-other-window "Other window")
  ("e" dumb-jump-go-prefer-external "Go external")
  ("x" dumb-jump-go-prefer-external-other-window "Go external other window")
  ("i" dumb-jump-go-prompt "Prompt")
  ("l" dumb-jump-quick-look "Quick look")
  ("b" dumb-jump-back "Back"))


;; ======== LSP ========
(use-package lsp-mode
  :hook ((js2-mode . lsp-deferred)
		 (c-mode . lsp-deferred)
		 (typescript-mode . lsp-deferred)
		 (css-mode . lsp-deferred)
		 (php-mode . lsp-deferred))
  :commands lsp lsp-deferred)

;; optionally
(use-package lsp-ui
  :requires lsp-mode flycheck
  :config
  (setq lsp-ui-doc-enable t
		lsp-ui-doc-use-childframe t
		lsp-ui-doc-position ‘top
		lsp-ui-doc-include-signature t
		lsp-ui-sideline-enable nil
		lsp-ui-flycheck-enable t
		lsp-ui-flycheck-list-position ‘right
		lsp-ui-flycheck-live-reporting t
		lsp-ui-peek-enable t
		lsp-ui-peek-list-width 60
		lsp-ui-peek-peek-height 25
		lsp-ui-sideline-enable nil)
  (add-hook 'lsp-mode-hook 'lsp-ui-mode))
(use-package company-lsp :commands company-lsp)
(use-package helm-lsp :commands helm-lsp-workspace-symbol)
(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

;; optionally if you want to use debugger
;; (use-package dap-mode)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;; old lsp code
;;   (add-hook 'typescript-mode-hook 'lsp)
;;   (add-hook 'js2-mode-hook 'lsp)
;;   (add-hook 'js-mode-hook 'lsp)
;;  )
;; ;; use typescript-language-server instead of javascript-typescript-langserver
;; (setq lsp-clients-typescript-server "typescript-language-server"
;;       lsp-clients-typescript-server-args '("--stdio"))
;;   ;; (add-hook 'typescript-mode-hook 'lsp)
;;   (add-hook 'js2-mode-hook 'lsp)
;;   (setq lsp-clients-typescript-server "typescript-language-server"
;;       lsp-clients-typescript-server-args '("--stdio"))
;; )
;; (use-package lsp-ui)
;; (use-package company-lsp)

;; (setq lsp-language-id-configuration '((java-mode . "java")
;;                                       (python-mode . "python")
;;                                       (gfm-view-mode . "markdown")
;;                                       (rust-mode . "rust")
;;                                       (css-mode . "css")
;;                                       (xml-mode . "xml")
;;                                       (c-mode . "c")
;;                                       (c++-mode . "cpp")
;;                                       (objc-mode . "objective-c")
;;                                       (web-mode . "html")
;;                                       (html-mode . "html")
;;                                       (sgml-mode . "html")
;;                                       (mhtml-mode . "html")
;;                                       (go-mode . "go")
;;                                       (haskell-mode . "haskell")
;;                                       (php-mode . "php")
;;                                       (json-mode . "json")
;;                                       (js2-mode . "javascript")
;;                                       (typescript-mode . "typescript")))


;; MERGE - should this still be here?  don't we declare this use-package stmt above?
(use-package lsp-mode
  :hook ((typescript-mode . lsp)
         (js2-mode . lsp)
         (js-mode . lsp)
         (c-mode . lsp))
  :commands lsp)
;; (use-package lsp-mode
;;   :hook (typescript-mode . lsp)
;;   :commands lsp)

;; optionally
(use-package lsp-ui :commands lsp-ui-mode)
(use-package company-lsp :commands company-lsp)
(use-package helm-lsp :commands helm-lsp-workspace-symbol)
(use-package lsp-treemacs :commands lsp-treemacs-errors-list)
;; optionally if you want to use debugger
(use-package dap-mode)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;; custom hydra I made for all lsp-enabled languages
(defhydra hydra-lsp (:color red
              :hint nil)
  "
  ^Buffer^                 ^Errors^                 ^Refactor^                   ^Find^                        ^GoTo/Jump^
-----------------------------------------------------------------------------------------------------------------------------------------
[_d_] Describe at point    [_e_] Flycheck          [_rs_] Rename symbol          [_fd_] Find definition        [_gi_] GoTo implementation              
[_bf_] Format buffer       [_lf_] LSP Flycheck                                   [_fi_] Find implementations   [_gt_] GoTo type def
[_i_] Info under point                                                           [_fr_] Find references        [_jn_] Jump to next ref
[_m_] iMenu                                                                      [_fs_] Find symbol            [_jp_] Jump to prev ref
  "
  ("d" lsp-describe-thing-at-point :exit t)
  ("bf" lsp-format-buffer :exit t)
  ("e" hydra-flycheck/body :exit t)
  ("fd" lsp-ui-peek-find-definitions :exit t)
  ("fi" lsp-ui-peek-find-implementation :exit t)
  ("fr" lsp-ui-peek-find-references :exit t)
  ("fs" lsp-ui-peek-find-workspace-symbol :exit t)
  ("gi" lsp-goto-implementation :exit t)
  ("gt" lsp-goto-type-definition :exit t)
  ("i" lsp-info-under-point :exit t)
  ("jn" lsp-ui-peek-jump-forward :exit t)
  ("jp" lsp-ui-peek-jump-backward :exit t)
  ("lf" lsp-ui-flycheck-list :exit t)
  ("m" lsp-ui-imenu :exit t)
  ("rs" lsp-rename :exit t)
  )


;; ======== YASNIPPET ========
(use-package yasnippet
  :diminish yas-minor-mode)
(yas-global-mode 1)
;; variable used in helper function to embed yas suggestions in company completion window
(defvar company-mode/enable-yas t)
;; snippet sources
(use-package yasnippet-snippets)
(use-package php-auto-yasnippets)
(setq php-auto-yasnippet-php-program "~/.emacs.d/config/Create-PHP-YASnippet.php")
(define-key php-mode-map (kbd "C-c C-y") 'yas/create-php-snippet)


;; ======== FLYCHECK ========
(use-package flycheck
  :defer t
  :init (global-flycheck-mode))


(defun lunaryorn-use-js-executables-from-node-modules ()
  "Set executables of JS checkers from local node modules."
  (-when-let* ((file-name (buffer-file-name))
         (root (locate-dominating-file file-name "node_modules"))
         (module-directory (expand-file-name "node_modules" root)))
  (pcase-dolist (`(,checker . ,module) '((javascript-jshint . "jshint")
                       (javascript-eslint . "eslint")
                       (javascript-jscs   . "jscs")))
    (let ((package-directory (expand-file-name module module-directory))
      (executable-var (flycheck-checker-executable-variable checker)))
    (when (file-directory-p package-directory)
      (set (make-local-variable executable-var)
         (expand-file-name (concat "bin/" module ".js")
                 package-directory)))))))

(defhydra hydra-flycheck
  (:pre (progn (setq hydra-lv t) (flycheck-list-errors))
    :post (progn (setq hydra-lv nil) (quit-windows-on "*Flycheck errors*"))
    :hint nil)
  "Errors"
  ("f"  flycheck-error-list-set-filter                            "Filter")
  ("j"  flycheck-next-error                                       "Next")
  ("k"  flycheck-previous-error                                   "Previous")
  ("gg" flycheck-first-error                                      "First")
  ("G"  (progn (goto-char (point-max)) (flycheck-previous-error)) "Last")
  ("q"  nil))


;; ======== GGTAGS ========
;;(use-package ggtags)
;;(add-hook 'c-mode-common-hook
;;	  (lambda ()
;;	    (when (derived-mode-p 'c-mode 'c++-mode 'java-mode 'asm-mode)
;;	      (ggtags-mode 1))))


;; ======== COUNSEL GTAGS ========
;; (use-package counsel-gtags)
;; ;; (add-hook 'c-mode-hook 'counsel-gtags-mode)
;; ;; (add-hook 'c++-mode-hook 'counsel-gtags-mode)
;; (add-hook 'flycheck-mode-hook 'counsel-gtags-mode)

;; (with-eval-after-load 'counsel-gtags
;;   (define-key counsel-gtags-mode-map (kbd "M-t") 'counsel-gtags-find-definition)
;;   (define-key counsel-gtags-mode-map (kbd "M-r") 'counsel-gtags-find-reference)
;;   (define-key counsel-gtags-mode-map (kbd "M-s") 'counsel-gtags-find-symbol)
;;   (define-key counsel-gtags-mode-map (kbd "M-,") 'counsel-gtags-go-backward))


;; ======== THEMES ========
;; (use-package solarized-theme)


;; ======== ALL THE ICONS ========
;; "Provides pretty icons, used by various themes (doom0-themes) and bar packages"
(use-package all-the-icons)
;; fix for lag on windows when using all-the-icons
(when (string-equal system-type "windows-nt") (setq inhibit-compacting-font-caches t))


;; ======== SPACELINE =========
;;(use-package spaceline)
;;(require 'spaceline-config)
;;(use-package spaceline-all-the-icons
;;  :after spaceline
;;  :config (spaceline-all-the-icons-theme))
;;(setq spaceline-all-the-icons-separator-type 'arrow) 

;; ======== POWERLINE ========
;; (use-package powerline
;;   :ensure t
;;   :config

;;   (defun make-rect (color height width)
;; 	"Create an XPM bitmap."
;; 	(when window-system
;; 	  (propertize
;; 	   " " 'display
;; 	   (let ((data nil)
;; 			 (i 0))
;; 		 (setq data (make-list height (make-list width 1)))
;; 		 (pl/make-xpm "percent" color color (reverse data))))))


;;   (defun powerline-mode-icon ()
;; 	(let ((icon (all-the-icons-icon-for-buffer)))
;; 	  (unless (symbolp icon) ;; This implies it's the major mode
;; 		(format " %s"
;; 				(propertize icon
;; 							'help-echo (format "Major-mode: `%s`" major-mode)
;; 							'face `(:height 1.3 :family ,(all-the-icons-icon-family-for-buffer))
;; 							'display '(raise 0.04))))))


;;   (setq-default mode-line-format 
;; 				'("%e"
;; 				  (:eval
;; 				   (let* ((active (powerline-selected-window-active))
;; 						  (modified (buffer-modified-p))
;; 						  (face1 (if active 'powerline-active1 'powerline-inactive1))
;; 						  (face2 (if active 'powerline-active2 'powerline-inactive2))
;; 						  (bar-color (cond ((and active modified) (face-foreground 'error))
;; 										   (active (face-background 'cursor))
;; 										   (t (face-background 'tooltip))))
;; 						  (lhs (list
;; 								(make-rect bar-color 45 3)
;; 								(when modified
;; 								  (concat
;; 								   " "
;; 								   (all-the-icons-faicon "floppy-o"
;; 														 :face (when active 'error)
;; 														 :v-adjust -0.01)))
;; 								" "
;; 								(powerline-buffer-id)
;; 								))
;; 						  (center (list
;; 								   " "
;; 								   (powerline-mode-icon)
;; 								   " "
;; 								   (powerline-major-mode)
;; 								   " "))
;; 						  (rhs (list
;; 								(format "%s" (eyebrowse--get 'current-slot))
;; 								" | "
;; 								(powerline-raw "%l:%c" 'face1 'r)
;; 								" | "
;; 								(powerline-raw "%6p" 'face1 'r)
;; 								(powerline-hud 'highlight 'region 1)
;; 								" "
;; 								))
;; 						  )
;; 					 (concat
;; 					  (powerline-render lhs)
;; 					  ;; changed bg color - variables changed from face1/face2 to bar-color 
;; 					  (powerline-fill-center bar-color (/ (powerline-width center) 1.0)) ;;changed from 2.0 - seems to center better on yoga3
;; 					  (powerline-render center)
;; 					  (powerline-fill bar-color (powerline-width rhs))
;; 					  (powerline-render rhs))))))
;; )


;; ======== PROJECTILE ========
(use-package projectile)
(use-package counsel-projectile
  :config
  (counsel-projectile-mode)
  (setq projectile-enable-caching t)
  ;; overwrite default projectile functions with counsel-projectile alternatives
  (define-key projectile-mode-map (kbd "C-c p b") 'counsel-projectile-switch-to-buffer)
  (define-key projectile-mode-map (kbd "C-c p b") 'counsel-projectile-switch-to-buffer)
  (define-key projectile-mode-map (kbd "C-c p d") 'counsel-projectile-find-dir)
  ;; currently overwriting projectile-find-file-in-directory (seems pointless)
  (define-key projectile-mode-map (kbd "C-c p l") 'counsel-projectile)
  (define-key projectile-mode-map (kbd "C-c p f") 'counsel-projectile-find-file)
  (define-key projectile-mode-map (kbd "C-c p p") 'counsel-projectile-switch-project)
  (define-key projectile-mode-map (kbd "C-c p s g") 'counsel-projectile-grep)
  (define-key projectile-mode-map (kbd "C-c p s s") 'counsel-projectile-ag))


;; ======== C-MODE ========
(add-hook 'c-mode-hook #'smartparens-mode)
(add-hook 'c-mode-hook #'evil-smartparens-mode)
(add-hook 'c-mode-hook #'linum-mode)
;; (setq-default c-basic-offset 4
;; 			  tab-width 4
;; 			  indent-tabs-mode t)
(sp-local-pair 'c-mode "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))

;;; cquery setup
(require 'cquery)
(setq cquery-executable "/usr/bin/cquery")
;; php-mode is based on c-mode, so we have to ensure it's not php-mode
(add-hook 'c-mode-common-hook (lambda ()
                                (when (not (string= (file-name-extension buffer-file-name) "php"))
                                  ;; MERGE - older version called (lsp-restart-workspace)
                                  (lsp-cquery-enable))))
;; MERGE - next line on primary PC for IupEmscripten but was commeented out
;; (setq cquery-extra-init-params '(:index (:comments 2) :cacheFormat "msgpack" :completion (:detailedLabel t)))
;; MERGE - next line uncommented on primary pc for IupEmscripten project
(setq cquery-extra-init-params '(:index (:comments 2) :cacheFormat "msgpack"))
(setq cquery-project-roots '("/home/matzy/Projects/IupEmscripten/src" "/home/matzy/Projects/IupEmscripten/include" "/home/matzy/Projects/IupEmscripten/src/emscripten"))
(setq cquery-extra-init-params '(:completion (:detailedLabel t) :extraClangArguments ("-I/home/matzy/Projects/IupEmscripten/src" "-I/usr/lib/emscripten/system/include" "-I/home/matzy/Projects/IupEmscripten/include" "-D__EMSCRIPTEN__")))
;;;;;;;; cquery setup
;; options include irony, cquery, rtags, ggtags, and ycmd
(use-package cquery
  ;; :after c-mode cc-mode objc-mode
  :commands (lsp-cquery-enable)
  :hook (c-mode-common . lsp-cquery-enable)
  :custom
  (cquery-executable "/Users/matzy/Development/cquery/BUILD/cquery")
  (cquery-project-roots '("/Users/matzy/Projects/IupEmscripten/src" "/Users/matzy/Projects/IupEmscripten/include" "/Users/matzy/Projects/IupEmscripten/src/emscripten"))
  (cquery-extra-init-params '(:index (:comments 2) :cacheFormat "msgpack"))
  (cquery-extra-init-params '(:completion (:detailedLabel t) :extraClangArguments ("-I/Users/matzy/Projects/IupEmscripten/src" "-I/Users/matzy/Development/emsdk_portable/emscripten/1.37.9/system/include" "-I/Users/matzy/Projects/IupEmscripten/include" "-D__EMSCRIPTEN__")))
  )

(use-package company-lsp
  :after company lsp-mode
  :custom (company-lsp-enable-recompletion t)
  :config
  (make-local-variable 'company-backends)
  (add-to-list 'company-backends 'company-lsp))

(use-package company-php
  :defer t)
(add-to-list 'auto-mode-alist '("\\.php\\'" . php-mode))
(add-hook 'php-mode-hook
      '(lambda ()
       (require 'company-php)
       (company-mode t)
       (ac-php-core-eldoc-setup) ;; enable eldoc
       (make-local-variable 'company-backends)
       (add-to-list 'company-backends 'company-ac-php-backend)))

;; mac IupEmscripten c setup
;; (setq cquery-extra-init-params '(:index (:comments 2) :cacheFormat "msgpack"))
;; (use-package cquery
;;   :after c-mode
;;   :commands (lsp-cquery-enable)
;;   :hook (c-mode-common . lsp-cquery-enable)
;;   :custom
;;   (cquery-executable "/usr/bin/cquery")
  ;; (cquery-project-roots '("/Users/matzy/Projects/IupEmscripten/src" "/Users/matzy/Projects/IupEmscripten/include" "/Users/matzy/Projects/IupEmscripten/src/emscripten"))
  ;; (cquery-extra-init-params '(:index (:comments 2) :cacheFormat "msgpack"))
  ;; (cquery-extra-init-params '(:completion (:detailedLabel t) :extraClangArguments ("-I/Users/matzy/Projects/IupEmscripten/src" "-I/Users/matzy/Development/emsdk_portable/emscripten/1.37.9/system/include" "-I/Users/matzy/Projects/IupEmscripten/include" "-D__EMSCRIPTEN__")))
  ;; )

(use-package ivy-xref
  :after ivy
  :custom (xref-show-xrefs-function #'ivy-xref-show-xrefs))

;; auto-insert include guards in header files
;; autoinsert C/C++ header
(define-auto-insert
  (cons "\\.\\([Hh]\\|hh\\|hpp\\)\\'" "My C/C++ header")
  '(nil
  (let* ((noext (substring buffer-file-name 0 (match-beginning 0)))
       (nopath (file-name-nondirectory noext))
       (ident (concat (upcase nopath) "_H_")))
    (concat "#ifndef " ident "\n"
        "#define " ident "\n\n\n"
        "\n\n#endif // " ident "\n"))
  ))

(add-hook 'find-file-hook 'auto-insert)

;; (optional) adds CC special commands to `company-begin-commands' in order to
;; trigger completion at interesting places, such as after scope operator
;;     std::|
;; (add-hook 'irony-mode-hook 'company-irony-setup-begin-commands)

;; Don't ask to reload TAGS if newer, just do it
;; (setq tags-revert-without-query 1)

(defhydra hydra-c (:color red
              :hint nil)
  "
_f_ flycheck
"
  ("f" hydra-flycheck/body :exit t)
  )
	;; (let* ((noext (substring buffer-file-name 0 (match-beginning 0)))
	;; 	   (nopath (file-name-nondirectory noext))
	;; 	   (ident (concat (upcase nopath) "_H_")))
	;;   (concat "#ifndef " ident "\n"
	;; 		  "#define " ident "\n\n\n"
	;; 		  "\n\n#endif // " ident "\n")))
(add-hook 'find-file-hook 'auto-insert)

;; Emacs GDB - Graphical Interface for GDB
(use-package gdb-mi :quelpa (gdb-mi :fetcher git
									:url "https://github.com/weirdNox/emacs-gdb.git"
									:files ("*.el" "*.c" "*.h" "Makefile"))
  :init
  (fmakunbound 'gdb)
  (fmakunbound 'gdb-enable-debug))

;; (use-package cmake-ide
;;   :config
;;   (cmake-ide-setup)
;;   )
;; (require 'cmake-ide)
;; (cmake-ide-setup)
;; ;; Set cmake-ide-flags-c++ to use C++11
;; (setq cmake-ide-cmake-opts "-DCMAKE_TOOLCHAIN_FILE=/Users/matzy/Development/emsdk_portable/emscripten/1.37.9/cmake/Modules/Platform/Emscripten.cmake -DCMAKE_BUILD_TYPE=Debug")
;; ;; (setq cmake-ide-flags-c (append '("-g4")))
;; (setq cmake-ide-build-dir "~/Projects/IupEmscripten/BUILD/emscripten")
;; ;; We want to be able to compile with a keyboard shortcut
;; (global-set-key (kbd "C-c m") 'cmake-ide-compile)


;; ======== ALL JAVASCRIPT ========
;; lsp server setup - works for all js modes - see hooks below
;; (use-package lsp-javascript-typescript
;;   :init
;;   (add-hook 'js2-mode-hook 'lsp-mode)
;;   (add-hook 'js2-mode-hook 'company-mode)
;;   (add-hook 'js2-mode-hook #'lsp-javascript-typescript-enable)

;;   (add-hook 'typescript-mode-hook 'lsp-mode)
;;   (add-hook 'typescript-mode-hook 'company-mode)
;;   (add-hook 'typescript-mode-hook #'lsp-javascript-typescript-enable)
;;   :config
;;   (defun my-company-transformer (candidates)
;;     (let ((completion-ignore-case t))
;;       (all-completions (company-grab-symbol) candidates)))
;;   (defun my-js-hook nil
;;     (make-local-variable 'company-transformers)
;;     (push 'my-company-transformer company-transformers))
;;   (add-hook 'js2-mode-hook 'my-js-hook)
;;   (add-hook 'typescript-mode-hook 'my-js-hook))

;; easily add jsdoc comments with C-c i
(use-package js-doc
  :ensure t
  :bind (:map js2-mode-map
              ("C-c i" . js-doc-insert-function-doc)
              ("@" . js-doc-insert-tag))
  :bind (:map typescript-mode-map
              ("C-c i" . js-doc-insert-function-doc)
              ("@" . js-doc-insert-tag))
  :config
  (setq js-doc-mail-address "cmatzenbach@rsna.org"
        js-doc-author (format "Chris Matzenbach<%s>" js-doc-mail-address)
        js-doc-license "MIT License"))


;; ======== JAVASCRIPT ========
(use-package js2-mode
  :defer t
  :init
  ;; (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode)) 
  (setq js2-include-node-externs t)
  (setq js2-include-browser-externs t)
  (setq js2-highlight-level 3)
  :config
  ;; better imenu
  (js2-imenu-extras-mode)
  ;; tern autocompletion
  ;; (use-package company-tern)
  ;; (add-to-list 'company-backends 'company-tern)
  (add-hook 'js2-mode-hook (lambda ()
							 ;; (tern-mode)
							 ;; (company-mode)
							 (smartparens-mode)
							 (evil-smartparens-mode)
							 (flycheck-mode)
							 (setq js2-basic-offset 4)
							 (linum-mode)))
  ;; (define-key tern-mode-keymap (kbd "M-.") nil)
  ;; (define-key tern-mode-keymap (kbd "M-,") nil)
  ;; js-mode (which js2 is based on) binds "M-." which conflicts with xref, so
  ;; unbind it.
  (define-key js-mode-map (kbd "M-.") nil))

;; javascript/typescript lsp - not bad but doesn't provide much that tide doesnt
;; (use-package lsp-javascript-typescript
;;   :init
;;   (add-hook 'js2-mode-hook 'lsp-mode)
;;   (add-hook 'js2-mode-hook 'company-mode)
;;   (add-hook 'js2-mode-hook #'lsp-javascript-typescript-enable)

;;   (add-hook 'typescript-mode-hook 'lsp-mode)
;;   (add-hook 'typescript-mode-hook 'company-mode)
;;   (add-hook 'typescript-mode-hook #'lsp-javascript-typescript-enable)
;;   :config
;;   (defun my-company-transformer (candidates)
;;  (let ((completion-ignore-case t))
;;    (all-completions (company-grab-symbol) candidates)))
;;   (defun my-js-hook nil
;;  (make-local-variable 'company-transformers)
;;  (push 'my-company-transformer company-transformers))
;;   (add-hook 'js2-mode-hook 'my-js-hook)
;;   (add-hook 'typescript-mode-hook 'my-js-hook)
;;   )

(use-package js2-highlight-vars
  :config (add-hook 'js2-mode-hook (lambda () (js2-highlight-vars-mode))))

(use-package js2-refactor
  :hook (js2-mode . js2-refactor-mode)
  :config 
  (js2r-add-keybindings-with-prefix "C-c C-r"))

(use-package xref-js2
  :defer t
  :config
  (define-key js2-mode-map (kbd "C-k") #'js2r-kill))

;; javascript REPL for testing code
(use-package nodejs-repl)

;; add-node-modules-path retrives binaries from node_modules for things like eslint
(use-package add-node-modules-path
  :hook (js2-mode . add-node-modules-path))

;;
(use-package js-comint
  :defer t)

;; setup mode hooks
(sp-local-pair 'js2-mode "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))
(add-hook 'js2-mode-hook (lambda ()
						   (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t)))

;; indium
(use-package indium
  :defer t
  :config
  (add-to-list 'evil-emacs-state-modes 'indium-repl-mode))

;; js2-refactor hydra
(defhydra hydra-js2-refactor (:color blue :hint nil)
  "
^Functions^                    ^Variables^               ^Buffer^                      ^sexp^               ^Debugging^
------------------------------------------------------------------------------------------------------------------------------
[_lp_] Localize Parameter      [_ev_] Extract variable   [_wi_] Wrap buffer in IIFE    [_k_]  js2 kill      [_lt_] log this
[_ef_] Extract function        [_iv_] Inline variable    [_ig_] Inject global in IIFE  [_ss_] split string  [_dt_] debug this
[_ip_] Introduce parameter     [_rv_] Rename variable    [_ee_] Expand node at point   [_sl_] forward slurp
[_em_] Extract method          [_vt_] Var to this        [_cc_] Contract node at point [_ba_] forward barf
[_ao_] Arguments to object     [_sv_] Split var decl.    [_uw_] unwrap
[_tf_] Toggle fun exp and decl [_ag_] Add var to globals
[_ta_] Toggle fun expr and =>  [_ti_] Ternary to if
[_z_] return                   [_q_]  quit"
  ("ee" js2r-expand-node-at-point)
  ("cc" js2r-contract-node-at-point)
  ("ef" js2r-extract-function)
  ("em" js2r-extract-method)
  ("tf" js2r-toggle-function-expression-and-declaration)
  ("ta" js2r-toggle-arrow-function-and-expression)
  ("ip" js2r-introduce-parameter)
  ("lp" js2r-localize-parameter)
  ("wi" js2r-wrap-buffer-in-iife)
  ("ig" js2r-inject-global-in-iife)
  ("ag" js2r-add-to-globals-annotation)
  ("ev" js2r-extract-var)
  ("iv" js2r-inline-var)
  ("rv" js2r-rename-var)
  ("vt" js2r-var-to-this)
  ("ao" js2r-arguments-to-object)
  ("ti" js2r-ternary-to-if)
  ("sv" js2r-split-var-declaration)
  ("ss" js2r-split-string)
  ("uw" js2r-unwrap)
  ("lt" js2r-log-this)
  ("dt" js2r-debug-this)
  ("sl" js2r-forward-slurp)
  ("ba" js2r-forward-barf)
  ("k" js2r-kill)
  ("q" nil)
  ("z" hydra-javascript/body)
  )

(defhydra hydra-javascript (:color red
                   :hint nil)
  "
  ^Buffer^                    ^Errors/Format^             ^Refactor^                   ^Indium^                 ^Tide^
---------------------------------------------------------------------------------------------------------------------------------------
[_d_]  Documentation         [_e_] Flycheck            [_rs_] Rename symbol         [_in_] Indium node       [_*_]  Restart server
[_fd_] Find definition       [_a_] Apply error fix     [_rf_] Refactor              [_ic_] Indium chrome     [_v_]  Verify setup
[_fr_] Find references       [_t_] Tide format         [_rj_] js2-refactor          [_is_] Indium scratch    [_oi_] Organize imports 
[_fj_] Jump to func def      [_c_] JSDoc comment
[_fw_] Show func def window
[_fx_] xref find refs
"
  ("d" tide-documentation-at-point :exit t)
  ("fd" tide-jump-to-definition :exit t)
  ("fr" tide-references :exit t)
  ("fj" xref-find-definitions)
  ("fw" xref-find-definitions-other-window)
  ("fx" xref-find-references)
  ("e" hydra-flycheck/body :exit t)
  ("a" tide-fix :exit t)
  ("t" tide-format :exit t)
  ("c" tide-jsdoc-template :exit t)
  ("rs" tide-rename-symbol :exit t)
  ("rf" tide-refactor :exit t)
  ("rj" hydra-js2-refactor/body :exit t)
  ("in" indium-connect-to-nodejs :exit t)
  ("ic" indium-connect-to-chrome :exit t)
  ("is" indium-scratch :exit t)
  ("*" tide-restart-server :exit t)
  ("v" tide-verify-setup :exit t)
  ("oi" tide-organize-imports :exit t)
  )

(defhydra hydra-js-lsp (:color red
                 :hint nil)
  "
  ^Buffer^                 ^Errors^                 ^Refactor^                   ^Find^                        ^GoTo/Jump^
-----------------------------------------------------------------------------------------------------------------------------------------
[_d_]  Describe at point    [_e_]  Flycheck         [_rs_] Rename symbol          [_fd_] Find definition        [_gi_] GoTo implementation              
[_bf_] Format buffer        [_lf_] LSP Flycheck     [_rj_] js2-refactor           [_fi_] Find implementations   [_gt_] GoTo type def
[_i_]  Info under point                                                       [_fr_] Find references        [_jn_] Jump to next ref
[_m_]  iMenu                                                                  [_fs_] Find symbol            [_jp_] Jump to prev ref
  "
  ("d" lsp-describe-thing-at-point :exit t)
  ("bf" lsp-format-buffer :exit t)
  ("e" hydra-flycheck/body :exit t)
  ("fd" lsp-ui-peek-find-definitions :exit t)
  ("fi" lsp-ui-peek-find-implementation :exit t)
  ("fr" lsp-ui-peek-find-references :exit t)
  ("fs" lsp-ui-peek-find-workspace-symbol :exit t)
  ("gi" lsp-goto-implementation :exit t)
  ("gt" lsp-goto-type-definition :exit t)
  ("i" lsp-info-under-point :exit t)
  ("jn" lsp-ui-peek-jump-forward :exit t)
  ("jp" lsp-ui-peek-jump-backward :exit t)
  ("lf" lsp-ui-flycheck-list :exit t)
  ("m" lsp-ui-imenu :exit t)
  ("rs" lsp-rename :exit t)
  ("rj" hydra-js2-refactor/body :exit t)
  )

(major-mode-hydra-bind js2-mode "Buffer"
  ("d" lsp-describe-thing-at-point)
  ("bf" lsp-format-buffer)
  ("i" lsp-info-under-point)
  ("m" lsp-ui-imenu)
  ("q" nil "quit"))
(major-mode-hydra-bind js2-mode "Errors"
  ("e" hydra-flycheck/body)
  ("lf" lsp-ui-flycheck-list))
(major-mode-hydra-bind js2-mode "Refactor/Test"
  ("rs" lsp-rename)
  ("rj" hydra-js2-refactor/body)
  ("re" nodejs-repl))
(major-mode-hydra-bind js2-mode "Find"
  ("fd" lsp-ui-peek-find-definitions)
  ("fi" lsp-ui-peek-find-implementation)
  ("fr" lsp-ui-peek-find-references)
  ("fs" lsp-ui-peek-find-workspace-symbol))
(major-mode-hydra-bind js2-mode "Go-to/Jump"
  ("gi" lsp-goto-implementation)
  ("gt" lsp-goto-type-definition)
  ("jn" lsp-ui-peek-jump-forward)
  ("jp" lsp-ui-peek-jump-backward))


;; ======== TYPESCRIPT ========
(use-package typescript-mode
  :defer t)
(add-hook 'typescript-mode-hook
	  '(lambda ()
	     (setq indent-tabs-mode nil)
	     (infer-indentation-style)))

(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  (company-mode +1)) 

(use-package tide
  :config
  (setq company-tooltip-align-annotations t)
  (setq tide-completion-detailed t)
  ;; (add-hook 'before-save-hook 'tide-format-before-save)
  (add-hook 'typescript-mode-hook #'setup-tide-mode)
  (add-hook 'rjsx-mode-hook #'setup-tide-mode)
  ;; MERGE - check if this should be commented out or not
  ;; (add-hook 'js2-mode-hook #'setup-tide-mode)
  (add-to-list 'company-backends 'company-tide)
  (setq js-indent-level 4)
  )

;; use tslint
(flycheck-add-mode 'typescript-tslint 'typescript-mode)

;; ;; configure smartparens
(add-hook 'typescript-mode-hook #'smartparens-mode)
(add-hook 'typescript-mode-hook #'evil-smartparens-mode)
(sp-local-pair 'typescript-mode "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))
(add-hook 'typescript-mode-hook #'add-node-modules-path)
(add-hook 'typescript-mode-hook #'linum-mode)

(defhydra hydra-typescript (:color red
                   :hint nil)
  "
  ^Buffer^                 ^Errors^                   ^Refactor^                   ^Format^                 ^Tide^
------------------------------------------------------------------------------------------------------------------------------------
[_d_]   Documentation      [_e_] Errors              [_rs_]  Rename symbol         [_t_]  Tide format       [_*_]  Restart server
[_fd_]  Find definition    [_a_] Apply error fix     [_rf_]  Refactor              [_c_]  JSDoc comment     [_v_]  Verify setup
[_fr_]  Find references                                                                               [_i_]  Organize imports 
"
  ("d" tide-documentation-at-point :exit t)
  ("fd" tide-jump-to-definition :exit t)
  ("fr" tide-references :exit t)
  ("c" tide-jsdoc-template :exit t)
  ("e" tide-project-errors :exit t)
  ("a" tide-fix :exit t)
  ("rs" tide-rename-symbol :exit t)
  ("rf" tide-refactor :exit t)
  ("t" tide-format :exit t)
  ("*" tide-restart-server :exit t)
  ("v" tide-verify-setup :exit t)
  ("i" tide-organize-imports :exit t)
  )

;; (major-mode-hydra-bind typescript-mode "Buffer"
;;   ("tf" tide-format)
;;   ("jd" tide-jsdoc-template)
;;   ("o" tide-organize-imports)
;;   ("q" nil "quit"))
;; (major-mode-hydra-bind typescript-mode "Errors"
;;   ("ef" hydra-flycheck/body)
;;   ("eh" tide-error-at-point)
;;   ("ep" tide-project-errors)
;;   ("ex" tide-fix)
;;   ("ed" tide-add-tslint-disable-next-line))
;; (major-mode-hydra-bind typescript-mode "Refactor"
;;   ("rs" tide-rename-symbol)
;;   ("rf" tide-rename-file)
;;   ("rr" tide-refactor))
;; (major-mode-hydra-bind typescript-mode "Find"
;;   ("d" tide-documentation-at-point)
;;   ("fr" tide-references))

(major-mode-hydra-bind typescript-mode "Buffer"
  ("d" lsp-describe-thing-at-point)
  ("bf" lsp-format-buffer)
  ("i" lsp-info-under-point)
  ("m" lsp-ui-imenu)
  ("q" nil "quit"))
(major-mode-hydra-bind typescript-mode "Errors"
  ("e" hydra-flycheck/body)
  ("lf" lsp-ui-flycheck-list))
(major-mode-hydra-bind typescript-mode "Refactor/Test"
  ("rs" lsp-rename)
  ("re" nodejs-repl))
(major-mode-hydra-bind typescript-mode "Find"
  ("fd" lsp-ui-peek-find-definitions)
  ("fi" lsp-ui-peek-find-implementation)
  ("fr" lsp-ui-peek-find-references)
  ("fs" lsp-ui-peek-find-workspace-symbol))
(major-mode-hydra-bind typescript-mode "Go-to/Jump"
  ("gi" lsp-goto-implementation)
  ("gt" lsp-goto-type-definition)
  ("jn" lsp-ui-peek-jump-forward)
  ("jp" lsp-ui-peek-jump-backward))


;; ======== REACT/JSX ========
(use-package rjsx-mode
  ;; currently have a hook in tide which uses tide for rjsx checking and completion - need jsconfig.json in root of project
  :defer t
  :init
  (add-to-list 'auto-mode-alist '("components\\/.*\\.js\\'" . rjsx-mode))
  (add-to-list 'auto-mode-alist '("containers\\/.*\\.js\\'" . rjsx-mode))
  :config
  (flycheck-add-mode 'javascript-eslint 'rjsx-mode)
  (flycheck-add-next-checker 'javascript-eslint 'jsx-tide 'append)
  (setq js2-strict-missing-semi-warning nil)
  (setq indent-tabs-mode t)
  (setq js-indent-level 4)
  (setq flycheck-disabled-checkers 'jsx-tide)
  (setq sgml-basic-offset 4)
  )

;; MERGE - both of these are declared elsewhere and should be unnecessary
;; add completion details
(setq tide-completion-detailed t)
;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)

;; configure smartparens
(add-hook 'rjsx-mode-hook #'smartparens-mode)
(add-hook 'rjxs-mode-hook #'evil-smartparens-mode)
(add-hook 'rjxs-mode-hook #'linum-mode)
(sp-local-pair 'rjsx-mode "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))
(add-hook 'rjsx-mode-hook 'linum-mode)
(add-hook 'rjsx-mode-hook 'add-node-modules-path)

(major-mode-hydra-bind rjsx-mode "Buffer"
  ("d" tide-documentation-at-point "Documentation")
  ("fd" tide-jump-to-definition "Find definition")
  ("fr" tide-references "Find references")
  ("q" nil "quit"))
(major-mode-hydra-bind rjsx-mode "Errors"
  ("e" hydra-flycheck/body)
  ("lf" tide-fix))
(major-mode-hydra-bind rjsx-mode "Refactor"
  ("rs" tide-rename-symbol)
  ("rf" tide-refactor)
  ("rj" hydra-js2-refactor/body))
(major-mode-hydra-bind rjsx-mode "Format"
  ("t" tide-format "Tide format")
  ("c" tide-jsdoc-template "JSDoc Comment"))
(major-mode-hydra-bind rjsx-mode "Tide"
  ("*" tide-restart-server)
  ("v" tide-verify-setup)
  ("i" tide-organize-imports))


;; ======== JSON ========
(use-package json-mode
  :mode (("\\.json\\'" . json-mode)
		 ("\\manifest.webapp\\'" . json-mode)
		 ("\\.tern-project\\'" . json-mode))
  :config (setq js2-basic-offset 4)
  :hook (json-mode . linum-mode))


;; ======== JADE ========
(use-package jade-mode
  :init (add-to-list 'auto-mode-alist '("\\.jade\\'" . jade-mode)))
(add-hook 'jade-mode-hook '(lambda ()
							 (linum-mode)
							 (setq indent-tabs-mode t)
							 (setq jade-tab-width 4)))

;; package for jade and stylus mode files (significant whitepsace mode)
(use-package sws-mode
  :defer t)

;; ======== JADE ========
(use-package jade-mode
  :init (add-to-list 'auto-mode-alist '("\\.jade\\'" . jade-mode)))
(add-hook 'jade-mode-hook '(lambda ()
                             (setq indent-tabs-mode t)
                             (setq jade-tab-width 4)))

;; ======== PHP ========
(use-package php-mode
  :defer t)
;; (use-package company-php
;;   :defer t
;;   :init
;; (add-to-list 'auto-mode-alist '("\\.php\\'" . php-mode)))
(add-hook 'php-mode-hook
      '(lambda ()
       (require 'company-php)
       (company-mode t)
       (ac-php-core-eldoc-setup) ;; enable eldoc
       (make-local-variable 'company-backends)
       (add-to-list 'company-backends 'company-ac-php-backend)
       (infer-indentation-style)))
; (use-package php-mode
;;   :mode
;;   (("[^.][^t][^p][^l]\\.php$" . php-mode))
;;   :defer t)
;; ;; (use-package company-php
;; ;;   :defer t)
;; (add-to-list 'auto-mode-alist '("\\.php\\'" . php-mode))
;; ;; (add-hook 'php-mode-hook
;; ;;           '(lambda ())
;; ;;              (require 'company-php)
;; ;;              (company-mode t)
;; ;;   :mode (("[^.][^t][^p][^l]\\.php$" . php-mode))
;; ;;   :defer t)
;; (use-package company-php
;;   :defer t)
;; ;; (add-to-list 'auto-mode-alist '("\\.php\\'" . php-mode))
;; (add-hook 'php-mode-hook
;;           '(lambda ()
;;              (require 'company-php)
;;              (company-mode t)
;; 			 (linum-mode)
;;              (ac-php-core-eldoc-setup) ;; enable eldoc
;;              ;; (make-local-variable 'company-backends)
;;              ;; (add-to-list 'company-backends 'company-ac-php-backend)
;;              (linum-on)))

;; configure smartparens
(add-hook 'php-mode-hook #'smartparens-mode)
(add-hook 'php-mode-hook #'evil-smartparens-mode)
(sp-local-pair 'php-mode "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))

;; configure lsp for php
;; (require 'lsp-php)
;; (add-hook 'php-mode-hook #'lsp-php-enable)

;; PHP Debugging with Xdebug
(use-package geben
  :config (setq geben-path-mappings '(("/home/matzy/Projects/phpapi.rsna.org" "/vagrant"))))

;; tool to test HTTP REST webservices
(use-package restclient
  :defer t)
(defun start-restclient ()
  (interactive)
  (let ((buf (generate-new-buffer "restclient")))
	(switch-to-buffer buf)
	(restclient-mode)
	buf))
;; beautiful use-package php config I found online
;; TODO: could use for php once the langserver is fixed, and could use as a template for other major modes using lsp
;; (use-package php-mode
;;   :mode
;;   (("[^.][^t][^p][^l]\\.php$" . php-mode))
;;   :config
;;   (add-hook 'php-mode-hook
;; 	    '(lambda ()
;; 	       (setq-local company-transformers '(company-sort-by-backend-importance))
;; 	       (set (make-local-variable 'company-backends)
;;                     '((company-lsp :with company-dabbrev-code)))

;; 	       ;;; LSP (Language Server Protocol) Settings:
;;                (add-to-list 'load-path "~/.emacs.d/lsp-php")
;;                (require 'lsp-php)
;; 	       (custom-set-variables
;; 	        	;; Composer.json detection after Projectile.
;; 	        	'(lsp-php-workspace-root-detectors (quote (lsp-php-root-projectile lsp-php-root-composer-json lsp-php-root-vcs)))
;; 	       )
;;                (lsp-php-enable))))

;; beautiful use-package php config I found online
;; TODO: could use for php once the langserver is fixed, and could use as a template for other major modes using lsp
;; (use-package php-mode
;;   :mode
;;   (("[^.][^t][^p][^l]\\.php$" . php-mode))
;;   :config
;;   (add-hook 'php-mode-hook
;; 	    '(lambda ()
;; 	       (setq-local company-transformers '(company-sort-by-backend-importance))
;; 	       (set (make-local-variable 'company-backends)
;;                     '((company-lsp :with company-dabbrev-code)))

;; 	       ;;; LSP (Language Server Protocol) Settings:
;;                (add-to-list 'load-path "~/.emacs.d/lsp-php")
;;                (require 'lsp-php)
;; 	       (custom-set-variables
;; 	        	;; Composer.json detection after Projectile.
;; 	        	'(lsp-php-workspace-root-detectors (quote (lsp-php-root-projectile lsp-php-root-composer-json lsp-php-root-vcs)))
;; 	       )
;;                (lsp-php-enable))))


;; ======== WEB MODE ========
(use-package web-mode
  :defer t)
(use-package company-web
  :defer t)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html\\.twig\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

;; (add-hook 'web-mode-hook (lambda ()
;;                            (set (make-local-variable 'company-backends) '(company-web-html))
;;                            (company-mode t)))
;; MERGE - should be working LSP function for HTML below
;; (add-hook 'web-mode-hook #'my-lsp-html-setup)
;; (use-package lsp-html)
;; (defun my-lsp-html-setup ()
;;   ;; need to change this to only act on HTML files
;;   (when (eq major-mode 'web-mode)
;;  (lsp-html-enable)))

(add-hook 'web-mode-hook #'smartparens-mode)
(add-hook 'web-mode-hook #'evil-smartparens-mode)
;;(add-to-list 'auto-mode-alist '("\\.html\\.twig\\'" . web-mode))
;;(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-hook 'web-mode-hook (lambda ()
						   (set (make-local-variable 'company-backends) '(company-web-html))))
(add-hook 'web-mode-hook #'smartparens-mode)
(add-hook 'web-mode-hook #'evil-smartparens-mode)
;; (add-hook 'web-mode-hook #'my-lsp-html-setup)
(add-hook 'web-mode-hook
          '(lambda ()
			 (linum-mode)
			 (setq indent-tabs-mode nil)
			 (infer-indentation-style)))


; ======== CSS MODE ========
(add-to-list 'auto-mode-alist '("\\.css\\'" . css-mode))
(add-hook 'css-mode-hook #'smartparens-mode)
(add-hook 'css-mode-hook #'evil-smartparens-mode)
(add-hook 'css-mode-hook
          '(lambda ()
			 (linum-mode)
			 (setq indent-tabs-mode nil)
			 (infer-indentation-style)))
(sp-local-pair 'css-mode "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))
;; (use-package 'lsp-css)
;; (defun my-css-mode-setup ()
;;   (interactive)
;;   (when (eq major-mode 'css-mode)
;;     ;; Only enable in strictly css-mode, not scss-mode (css-mode-hook fires for scss-mode)
;;     (lsp-css-enable)))
;; (add-hook 'css-mode-hook #'my-css-mode-setup)
;; (add-hook 'less-css-mode-hook #'lsp-less-enable)
;; (add-hook 'sass-mode-hook #'lsp-scss-enable)
;; (add-hook 'scss-mode-hook #'lsp-scss-enable)


;; ======== LESS MODE ========
(use-package less-css-mode
  :init (add-to-list 'auto-mode-alist '("\\.less$" . less-css-mode))
  :hook (less-css-mode . lsp-less-enable)
  (less-css-mode . smartparens-mode)
  (less-css-mode . evil-smartparens-mode)
  (less-css-mode . linum-mode))


;; ======== SQL IDE ========
;; windows only - fix to make emacs+mysql work
;; (setq sql-mysql-options '("-C" "-t" "-f" "-n"))
;; set up connection list
(setq sql-connection-alist
    '((casesdev (sql-product 'mysql)
          (sql-port 3306)
          (sql-server "dev.cluster-cpqhbit12kdd.us-east-1.rds.amazonaws.com")
          (sql-user "rsnards")
          (sql-password "RSNA1915")
          (sql-database "case_repository"))
    (casesstage (sql-product 'mysql)
          (sql-port 3306)
          (sql-server "stage.cluster-cpqhbit12kdd.us-east-1.rds.amazonaws.com")
          (sql-user "rsnards")
          (sql-password "RSNA1915")
          (sql-database "case_repository"))
    (atlantis (sql-product 'psql)
          (sql-port 5432)
          (sql-server "localhost")
          (sql-user "matzy")
          (sql-password "birdie")
          (sql-database "atlantis"))))

(defun mysql-cases-dev ()
  "Connect to casesdev database."
  (interactive)
  (my-sql-connect 'mysql 'casesdev))

(defun mysql-cases-stage ()
  "Connect to casesstage database."
  (interactive)
  (my-sql-connect 'mysql 'casesstage))

(defun postgres-atlantis-dev ()
  "Connect to atlantis dev database."
  (interactive)
  (my-sql-connect 'psql 'atlantis))

(defun my-sql-connect (product connection)
  "Connect to custom-defined databases."
  ;; remember to set sql product, otherwise it will fail for the first time you call the function
  (setq sql-product product)
  (sql-connect connection))


;; ======== ELISP ========
;; highlights elisp symbols
(use-package highlight-defined
  :hook (emacs-lisp-mode-hook . highlight-defined-mode))
;; enable smartparens in strict mode
(add-hook 'emacs-lisp-mode-hook #'smartparens-mode)
(add-hook 'emacs-lisp-mode-hook #'smartparens-strict-mode)
;; also use cleverparens on top of it
(add-hook 'emacs-lisp-mode-hook #'evil-cleverparens-mode)
(add-hook 'emacs-lisp-mode-hook 'linum-mode)
;; minor mode for highlighting lisp quotes and quoted symbols (locally installed package)
(add-hook 'emacs-lisp-mode-hook #'highlight-quoted-mode)
(add-hook 'emacs-lisp-mode-hook #'linum-mode)
(add-hook 'emacs-lisp-mode-hook
;; lol at the function below
;(lambda ()
;  (define-key emacs-lisp-mode-map "\C-x\C-e" 'pp-eval-last-sexp)
;  (setq tab-width 2)))
(lambda ()
  (define-key emacs-lisp-mode-map "\C-x\C-e" 'pp-eval-last-sexp)))

;; testing - suggest.el https://github.com/Wilfred/suggest.el
(use-package suggest
  :defer t)

;; eldoc - provides minibuffer hints when working in elisp
(use-package "eldoc"
  :diminish eldoc-mode
  :commands turn-on-eldoc-mode
  :defer t
  :init
  (progn
	(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
	(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
	(add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)))

;; major mode hydra
(major-mode-hydra-bind emacs-lisp-mode "Eval"
  ("eb" eval-buffer "Eval buffer")
  ("er" eval-region "Eval region")
  ("es" pp-eval-last-sexp "Eval sexp")
  ("t" edebug-all-defs "Toggle defs eval"))
(major-mode-hydra-bind emacs-lisp-mode "Errors/REPL"
  ("f" hydra-flycheck/body "Flycheck menu")
  ("i" ielm))
(major-mode-hydra-bind emacs-lisp-mode "Edebug"
  ("di" edebug-set-initial-mode "Set initial mode")
  ("dg" edebug-go-mode "Go mode")
  ("dw" edebug-where "Where")
  ("dn" edebug-next-mode "Next mode")
  ("ds" edebug-step-mode "Step mode"))
(major-mode-hydra-bind emacs-lisp-mode "Exit"
  ("q" nil "Quit"))

;; manual hydra
(defhydra hydra-edebug (:color amaranth
							   :hint  nil)
  "
	EDEBUG MODE
^^_<SPC>_ step             ^^_f_ forward sexp         _b_reakpoint set                previous _r_esult      _w_here                    ^^_d_ebug backtrace
^^_n_ext                   ^^goto _h_ere              _u_nset breakpoint              _e_val expression      bounce _p_oint             _q_ top level (_Q_ nonstop)
_g_o (_G_ nonstop)         ^^_I_nstrument callee      next _B_reakpoint               _E_val list            _v_iew outside             ^^_a_bort recursive edit
_t_race (_T_ fast)         step _i_n/_o_ut            _x_ conditional breakpoint      eval _l_ast sexp       toggle save _W_indows      ^^_S_top
_c_ontinue (_C_ fast)      ^^^^                       _X_ global breakpoint
"
  ("<SPC>" edebug-step-mode)
  ("n"     edebug-next-mode)
  ("g"     edebug-go-mode)
  ("G"     edebug-Go-nonstop-mode)
  ("t"     edebug-trace-mode)
  ("T"     edebug-Trace-fast-mode)
  ("c"     edebug-continue-mode)
  ("C"     edebug-Continue-fast-mode)

  ("f"     edebug-forward-sexp)
  ("h"     edebug-goto-here)
  ("I"     edebug-instrument-callee)
  ("i"     edebug-step-in)
  ("o"     edebug-step-out)

  ;; breakpoints
  ("b"     edebug-set-breakpoint)
  ("u"     edebug-unset-breakpoint)
  ("B"     edebug-next-breakpoint)
  ("x"     edebug-set-conditional-breakpoint)
  ("X"     edebug-set-global-break-condition)

  ;; evaluation
  ("r"     edebug-previous-result)
  ("e"     edebug-eval-expression)
  ("l"     edebug-eval-last-sexp)
  ("E"     edebug-visit-eval-list)

  ;; views
  ("w"     edebug-where)
  ("p"     edebug-bounce-point)
  ("v"     edebug-view-outside) ; maybe obsolete??
  ("P"     edebug-view-outside) ; same as v
  ("W"     edebug-toggle-save-windows)

  ("d"     edebug-backtrace)

  ;; quitting and stopping
  ("q"     top-level :color blue)
  ("Q"     edebug-top-level-nonstop :color blue)
  ("a"     abort-recursive-edit :color blue)
  ("S"     edebug-stop :color blue))
(with-eval-after-load 'edebug
  (bind-key "?" #'hydra-edebug/body edebug-mode-map))


;; ======== COMMON LISP ========
;; (load (expand-file-name "~/quicklisp/slime-helper.el"))
;; Replace "sbcl" with the path to your implementation
(setq inferior-lisp-program "/usr/bin/sbcl")
(add-to-list 'auto-mode-alist '("\\.lisp\\'\\|\\.cl\\'" . lisp-mode)) 
;; keep evil and emacs bindings for M-.
(define-key evil-normal-state-map (kbd "M-.")
  `(menu-item "" evil-repeat-pop :filter
			  ,(lambda (cmd) (if (eq last-command 'evil-repeat-pop) cmd))))

(use-package slime
  :defer t)
(add-hook 'lisp-mode-hook (lambda ()
							(slime-mode t)
							(linum-mode)
							(use-package slime-company)
							(company-mode t)
							(linum-mode)))

(slime-setup '(slime-fancy slime-company))

(add-hook 'lisp-mode-hook #'smartparens-mode)
(add-hook 'lisp-mode-hook #'smartparens-strict-mode)
(add-hook 'lisp-mode-hook #'evil-cleverparens-mode)
(add-hook 'lisp-mode-hook #'highlight-quoted-mode)
(setq inferior-lisp-program "sbcl --dynamic-space-size 2048")

;; keep evil and emacs bindings for M-.
(define-key evil-normal-state-map (kbd "M-.")
  `(menu-item "" evil-repeat-pop :filter
			  ,(lambda (cmd) (if (eq last-command 'evil-repeat-pop) cmd))))

;;; Set of hydra menus to work with Slime.
;;; vindarel 2017
;;; copyleft
(defhydra slime-hydra (:color red)
  "
	 Slime
"
  ("." slime-edit-definition "edit definition")
  ("," slime-pop-find-definition-stack "return from definition")
  ("s" slime-selector-hydra/body "selector" :color blue)
  ;; evaluation
  ;; debugging
  ;; compilation
  ;; cross reference
  ;; editing
  ;; …
  )

(defun slime-selector-call-by-key (key)
  "Call a slime-selector function associated with the given KEY."
  ;; Strangely, this code is obscured in slime.el. Functions are not
  ;; defined by name.
  (funcall (cl-third (cl-find key slime-selector-methods :key #'car))))

(defhydra slime-selector-hydra (:color red
									   :columns 4)
  " Slime selector "
  ("4" (slime-selector-call-by-key ?4) "other window")
  ("c" (slime-selector-call-by-key ?c) "connections buffer")
  ("d" (slime-selector-call-by-key ?d) "*sldb* buffer for the current connection")
  ("e" (slime-selector-call-by-key ?e) "most recently emacs-lisp-mode buffer")
  ("i" (slime-selector-call-by-key ?i) "*inferior-lisp* buffer")
  ("l" (slime-selector-call-by-key ?l) "most recently visited lisp-mode buffer")
  ("n" (slime-selector-call-by-key ?n) "next Lisp connection")
  ("p" (slime-selector-call-by-key ?p) "previous Lisp connection")
  ("r" (slime-selector-call-by-key ?r) "REPL")
  ("s" (slime-selector-call-by-key ?s) "*slime-scratch* buffer")
  ("t" (slime-selector-call-by-key ?t) "threads buffer")
  ("v" (slime-selector-call-by-key ?v) "*slime-events* buffer")
  ("q" nil "quit")
  ("S" slime-hydra/body "Slime hydra" :color blue)
  )


;; TODO: Add Debug Hydra
(defhydra hydra-slime-compile (:color blue :columns 3)
  "Compile"
  ("c" slime-compile-file "Compile")
  ("C" slime-compile-and-load-file "Compile and Load")
  ("l" slime-load-file "Load File")
  ("f" slime-compile-defun "Compile Defun")
  ("r" slime-compile-region "Compile Region")
  ("n" slime-remove-notes "Remove Notes"))

(defhydra hydra-slime-eval (:color blue :columns 3)
  "Eval"
  ("b" slime-eval-buffer "Buffer")
  ("f" slime-eval-defun "Defun")
  ("F" slime-undefine-function "Undefine Function")
  ("e" slime-eval-last-expression "Last Sexp")
  ("r" slime-eval-region "Region"))

(defhydra hydra-slime-help (:color blue :columns 3)
  "Help"
  ("a" slime-apropos "Apropros")
  ("A" slime-apropos-all "Apropros All")
  ("d" slime-disassemble-symbol "Disassemble")
  ("h" slime-describe-symbol "Describe Symbol")
  ("H" slime-hyperspec-lookup "Hyperspec Lookup")
  ("p" slime-apropos-package "Apropos Package")
  ("t" slime-toggle-trace-fdefinition "Toggle Trace Fdefinition")
  ("T" slime-untrace-all "Untrace All")
  ("<" slime-who-calls "Who Calls")
  (">" slime-calls-who "Calls Who")
  ;; TODO: Add key bindings for who binds/sets globals?
  ("r" slime-who-references "Who References")
  ("m" slime-who-macroexpands "Who Macroexpands")
  ("s" slime-who-specializes "Who Specializes"))

(defhydra hydra-slime-navigate (:color blue :columns 3)
  "Navigate"
  ("g" slime-edit-definition "Find Definition")
  ("b" slime-pop-find-definition-stack "Find Definition Pop")
  ("n" slime-next-note "Next Note")
  ("p" slime-previous-note "Previous Note")
  ("r" slime-who-references "Who References")
  ("m" slime-who-macroexpands "Who Macroexpands")
  ("s" slime-who-specializes "Who Specializes"))

(defhydra hydra-slime-mode (:color blue :columns 3)
  "Slime"
  ("e" hydra-slime-eval/body "Eval")
  ("h" hydra-slime-help/body "Help")
  ("g" hydra-slime-navigate/body "Navigate")
  ("x" slime-scratch "Scratch")
  ("ma" slime-macroexpand-all "Macroexpand All")
  ("mo" slime-macroexpand-1 "Macroexpand One")
  ("se" slime-eval-last-expression-in-repl "Eval Last Expression in Repl")
  ("si" slime "Slime")
  ("sq" slime-quit-lisp "Quit Lisp")
  ("tf" slime-toggle-fancy-trace "Toggle Fancy Trace"))


;; ======== RACKET ========
(use-package racket-mode
  :defer t)
;; TODO wouldn't quack interfere with racket mode? i had them both active without problem seemingly...
;; (use-package quack
;;   :after racket-mode)

;; on MacOS, need to manually point to racket binary
(if (string-equal system-type "darwin")
	(setq racket-program "/Applications/Racket v6.9/bin/racket"))

(use-package scribble-mode
  :defer t)
(add-to-list 'auto-mode-alist '("\\.rkt\\'" . racket-mode)) 

;; setup smartparens and cleverparens
(add-hook 'racket-mode-hook #'smartparens-mode)
(add-hook 'racket-mode-hook #'smartparens-strict-mode)
(add-hook 'racket-mode-hook #'evil-cleverparens-mode)
(add-hook 'racket-mode-hook #'linum-mode)
(add-hook 'racket-mode-hook #'highlight-quoted-mode)


;; ======== MAGIT ========
(use-package evil-magit)

;; full screen magit-status
(defadvice magit-status (around magit-fullscreen activate)
  (window-configuration-to-register :magit-fullscreen)
  ad-do-it
  (delete-other-windows))

(defun magit-quit-session ()
  "Restores the previous window configuration and kills the magit buffer"
  (interactive)
  (kill-buffer)
  (jump-to-register :magit-fullscreen))

(define-key magit-status-mode-map (kbd "q") 'magit-quit-session)


;; ======== HELPFUL ========
(use-package helpful)


;; ======== PACKAGE-MENU ========
;; hydra for emacs package menu
(defhydra hydra-package-menu (:color pink :hint nil)
  "
_k_↑ _p_rev    _U_pgrade      _d_elete   _f_ilter _H_ide       _r_efresh
_j_↓ _n_ext    _~_: obsolete  _i_nstall  _S_ort   _(_: toggle  _g_: revert
_<_  _>_       _z_: execute   _?_: info  _u_nmark _q_uit
"
  ("SPC" nil)
  ("(" package-menu-toggle-hiding)
  ("<" beginning-of-buffer)
  (">" end-of-buffer)
  ("?" package-menu-describe-package)
  ("H" package-menu-hide-package)
  ("S" tabulated-list-sort)
  ("U" package-menu-mark-upgrades)
  ("d" package-menu-mark-delete)
  ("f" package-menu-filter)
  ("g" revert-buffer)
  ("i" package-menu-mark-install)
  ("n" next-line)
  ("p" previous-line)
  ("q" quit-window :color blue)
  ("r" package-menu-refresh)
  ("u" package-menu-mark-unmark)
  ("z" package-menu-execute)
  ("~" package-menu-mark-obsolete-for-deletion)
  ("j" scroll-up-command)
  ("k" scroll-down-command))
(bind-keys :map package-menu-mode-map
		   ("SPC" . hydra-package-menu/body)
		   ("z" . package-menu-execute)
		   ("x" . god-mode-self-insert)
		   ("a" . god-mode-self-insert)
		   ("e" . god-mode-self-insert)
		   ("j" . scroll-up-command)
		   ("k" . scroll-down-command))


;; ======== SMERGE MODE ========
;; built in emacs mode for source control file merges
(use-package smerge-mode
  :bind ("C-x m" . hydra-smerge/body)
  :config
  (defhydra hydra-smerge
	(:color red :hint nil :pre (smerge-start-session))
	"
^Move^‗‗‗‗‗‗^Keep^‗‗‗‗‗‗‗‗‗^Diff^‗‗‗‗‗‗^Pair^‗‗‗‗‗‗‗‗‗‗
_n_ext      _b_ase         _R_efine    _<_: base-upper
_p_rev      _l_ower        _E_diff     _=_: upper-lower
^ ^         _u_pper        _C_ombine   _>_: base-lower
^ ^         _a_ll          _r_esolve
_q_uit      _RET_: current
"
	("RET" smerge-keep-current)
	("C"   smerge-combine-with-next)
	("E"   smerge-ediff)
	("R"   smerge-refine)
	("a"   smerge-keep-all)
	("b"   smerge-keep-base)
	("l"   smerge-keep-lower)
	("n"   smerge-next)
	("p"   smerge-prev)
	("r"   smerge-resolve)
	("u"   smerge-keep-upper)
	("<"   smerge-diff-base-upper)
	("="   smerge-diff-upper-lower)
	(">"   smerge-diff-base-lower)
	("q" nil :color blue)))


;; ======== UNDO TREE ========
(use-package undo-tree
  :diminish undo-tree-mode
  :config
  (progn
	(global-undo-tree-mode)
	(setq undo-tree-visualizer-timestamps t)
	(setq undo-tree-visualizer-diff t)))


;; ======== MARKDOWN ========
(when (use-package markdown-mode)
  (add-to-list 'auto-mode-alist '("\\.md\\.html\\'" . markdown-mode))
  (after-load 'whitespace-cleanup-mode
			  push 'markdown-mode whitespace-cleanup-mode-ignore-modes)
			  (push 'markdown-mode whitespace-cleanup-mode-ignore-modes))

(defhydra hydra-markdown (:hint nil)
  "
Formatting        C-c C-s    _s_: bold          _e_: italic     _b_: blockquote   _p_: pre-formatted    _c_: code

Headings          C-c C-t    _h_: automatic     _1_: h1         _2_: h2           _3_: h3               _4_: h4

Lists             C-c C-x    _m_: insert item   

Demote/Promote    C-c C-x    _l_: promote       _r_: demote     _u_: move up      _d_: move down

Links, footnotes  C-c C-a    _L_: link          _U_: uri        _F_: footnote     _W_: wiki-link      _R_: reference
 
"


  ("s" markdown-insert-bold)
  ("e" markdown-insert-italic)
  ("b" markdown-insert-blockquote :color blue)
  ("p" markdown-insert-pre :color blue)
  ("c" markdown-insert-code)

  ("h" markdown-insert-header-dwim) 
  ("1" markdown-insert-header-atx-1)
  ("2" markdown-insert-header-atx-2)
  ("3" markdown-insert-header-atx-3)
  ("4" markdown-insert-header-atx-4)

  ("m" markdown-insert-list-item)

  ("l" markdown-promote)
  ("r" markdown-demote)
  ("d" markdown-move-down)
  ("u" markdown-move-up)  

  ("L" markdown-insert-link :color blue)
  ("U" markdown-insert-uri :color blue)
  ("F" markdown-insert-footnote :color blue)
  ("W" markdown-insert-wiki-link :color blue)
  ("R" markdown-insert-reference-link-dwim :color blue) 
  )


;; ======== ORG-MODE ========
(use-package org
  :ensure org-plus-contrib
  :mode ("\\.org\\'" . org-mode))
(setq org-M-RET-may-split-line nil)
;; make lists expanded upon opening org files
(setq org-startup-folded nil)
;; hide emphasis markup (/.../ for italics, *...* for bold, etc)
(setq org-hide-emphasis-markers t)
;; get code block syntax highlighting in HTML export
(setq org-src-fontify-natively t)
;; set states for tasks
(setq org-todo-keywords '(
						  (sequence "TODO(t!)" "NEXT(n!)" "STARTED(a!)" "WAIT(w@/!)" "OTHERS(o!)" "|" "DONE(d)" "CANCELLED(c)")
						  ))
;; replace any lists using -'s with bullets
(font-lock-add-keywords 'org-mode
						'(("^ *\\([-]\\) "
						   (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
;; make text wrap around size of window
(add-hook 'org-mode-hook 'visual-line-mode)

;; auto-org-md - automatically export a markdown file when saving an org file
(use-package auto-org-md
  :defer t)

;; use uuid's for org links
(require 'org-id)
(setq org-id-link-to-org-use-id 'create-if-interactive)

;; beautify org-mode
(use-package org-bullets
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
(setq org-ellipsis "⤵")
;; adjusts number of spacing around headlines
(setq org-ascii-headline-spacing (quote (1 . 1)))

(setq org-default-notes-file "~/Dropbox/org/notes.org")
(setq org-capture-templates
    '(
    ("t" "Todo" entry (file+headline "~/Dropbox/org/inbox.org" "Tasks")
     "* TODO %?\n  %i\n  %u\n  %a")
    ("n" "Note/Data" entry (file+headline "~/Dropbox/org/inbox.org" "Notes/Data")
     "* %?   \n  %i\n  %u\n  %a")
    ("j" "Journal" entry (file+datetree "~/Dropbox/org/journal.org")
     "* %?\nEntered on %U\n %i\n %a")
    ("J" "Work-Journal" entry (file+datetree "~/Dropbox/org/wjournal.org")
     "* %?\nEntered on %U\n %i\n %a")
    ))
(setq org-irc-link-to-logs t)

;; note time when something is marked as complete
(setq org-log-done 'time)

;; configure which files to use in org-agenda
(setq org-agenda-files (list "~/Dropbox/org/inbox.org"
;              ;; "~/Dropbox/org/email.org"
;              "~/Dropbox/org/tasks.org"
;              "~/Dropbox/org/wtasks.org"
;              "~/Dropbox/org/journal.org"
;              "~/Dropbox/org/wjournal.org"
;              ;; "~/Dropbox/org/kb.org"
;              ;; "~/Dropbox/org/wkb.org"
;              "~/Dropbox/org/time-tracking-bgp.org"
;              ))
;setq org-agenda-text-search-extra-files
;   (list "~/Dropbox/org/someday.org"
;      "~/Dropbox/org/config.org"
;      ))
;
;(setq org-refile-targets '((nil :maxlevel . 2)
;               (org-agenda-files :maxlevel . 2)
;               ("~/Dropbox/org/someday.org" :maxlevel . 2)
;               ("~/Dropbox/org/templates.org" :maxlevel . 2)
;;               )
;    )
;(setq org-outline-path-complete-in-steps nil)         ; Refile in a single go
;(setq org-refile-use-outline-path 'file)

;; MobileOrg setup
							 ;; "~/Dropbox/org/email.org"
							 "~/Dropbox/org/todo.org"
							 "~/Dropbox/org/projects.org"
							 ))
(setq org-agenda-text-search-extra-files
	  (list "~/Dropbox/org/notes.org"
			"~/Dropbox/org/time-tracking-bgp.org"
			"~/Dropbox/org/templates.org"
			))
(setq org-agenda-custom-commands
	  '(("c" "Simple agenda view"
		 ((agenda "")
		  (todo "")))))


(setq org-refile-targets '((nil :maxlevel . 2)
						   (org-agenda-files :maxlevel . 2)
						   ("~/Dropbox/org/notes.org" :maxlevel . 2)
						   ;; ("~/Dropbox/org/templates.org" :maxlevel . 2)
						   )
	  )
(setq org-outline-path-complete-in-steps nil)         ; Refile in a single go
(setq org-refile-use-outline-path 'file)

;; org-babel setup
(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (emacs-lisp . t)
   (org . t)
   ;; (sh . t)
   (C . t)
   (python . t)
   (gnuplot . t)
   (octave . t)
   (R . t)
   (dot . t)
   (awk . t)
   ))
;; displays contents of code blocks in major mode of that langauge
(setq org-src-fontify-natively t)
;; makes tab behave in code blocks as it does in corrosponding major mode
(setq org-src-tab-acts-natively t)
;; don't eval code blocks during export
(setq org-export-babel-evaluate nil)

;; function to archive finished tasks
(defun org-archive-done-tasks ()
  (interactive)
  (org-map-entries
   (lambda ()
	 (org-archive-subtree)
	 (setq org-map-continue-from (outline-previous-heading)))
   "/DONE" 'file)
  (org-map-entries
   (lambda ()
	 (org-archive-subtree)
	 (setq org-map-continue-from (outline-previous-heading)))
   "/CANCELLED" 'file))

;; MobileOrg Setup
;; set default directory for org files
(setq org-directory "~/Dropbox/org")
;; set the name of the file where new notes will be stored
(setq org-mobile-inbox-for-pull "~/Dropbox/org/inbox.org")
;; set MobileOrg directory so org-mobile can push all org files there
(setq org-mobile-directory "~/Dropbox/Apps/MobileOrg")

;; Org Trello (not working, commented on issue with bug report TODO)
(use-package org-trello
  :defer t)
;; (require 'org-trello)

;; Org Jira (not working, talk to SR?)
(use-package org-jira
  :defer t
  :config
  (setq jiralib-url "https://jira.rsna.org/"))

;; hydra for org-mode
(defhydra hydra-org-mode (:color blue)
 "
Org Mode (_q_uit)

^Clock^       ^Editing^                   ^Date^             ^Display^
^-----^----   ^-------^-----------------  ^----^-----------  ^-------^------------
_ci_ in       _mh_ modify heading type     _ds_ set schedule  _w_ toggle line wrap
_co_ out      _mi_ modify item type        _dd_ set deadline  _*_ show whole file
_cq_ cancel   _mt_ modify todo state       _dt_ timestamp     _r_ org-reveal
_cs_ summary  _so_ sort subtree/reg/list   ^^   do later      ^^
_cj_ jump     _st_ set tag for heading     ^^   do earlier    ^^
^^            _is_ insert sparse tree      ^^                 ^^
^^            _it_ insert table            ^^                 ^^
 "
)

(major-mode-hydra-bind org-mode "Clock"
  ("ci" org-clock-in "in")
  ("co" org-clock-out "out")
  ("cq" org-clock-cancel "cancel")
  ("cs" org-clock-display "summary")
  ("cj" org-clock-goto "jump"))
(major-mode-hydra-bind org-mode "Editing"
  ("mh" org-ctrl-c-star "modify heading type")
  ("mi" org-ctrl-c-minus "modify item type")
  ("mt" org-todo "modify todo state")
  ("so" org-sort "sort subtree/reg/list")
  ("st" org-set-tags-command "set tag for heading")
  ("is" org-sparse-tree "insert sparse tree")
  ("it" org-table-create "insert table"))
(major-mode-hydra-bind org-mode "Date"
  ("ds" org-schedule "set schedule")
  ("dd" org-deadline "set deadline")
  ("dt" org-time-stamp "timestamp"))
(major-mode-hydra-bind org-mode "Display"
  ("w" toggle-truncate-lines "toggle line wrap")
  ("*" display-entire-org-file "show whole file")
  ("r" org-reveal "org-reveal")
  ("q" nil "quit"))

;; Hydra for org agenda (graciously taken from Spacemacs)
(defhydra hydra-org-agenda (:pre (setq which-key-inhibit t)
								 :post (setq which-key-inhibit nil)
								 :hint none)
  "
Org agenda (_q_uit)

^Clock^      ^Visit entry^              ^Date^             ^Other^
^-----^----  ^-----------^------------  ^----^-----------  ^-----^---------
_ci_ in      _SPC_ in other window      _ds_ schedule      _gr_ reload
_co_ out     _TAB_ & go to location     _dd_ set deadline  _._  go to today
_cq_ cancel  _RET_ & del other windows  _dt_ timestamp     _gd_ go to date
_cj_ jump    _o_   link                 _+_  do later      ^^
^^           ^^                         _-_  do earlier    ^^
^^           ^^                         ^^                 ^^
^View^          ^Filter^                 ^Headline^         ^Toggle mode^
^----^--------  ^------^---------------  ^--------^-------  ^-----------^----
_vd_ day        _ft_ by tag              _ht_ set status    _tf_ follow
_vw_ week       _fr_ refine by tag       _hk_ kill          _tl_ log
_vt_ fortnight  _fc_ by category         _hr_ refile        _ta_ archive trees
_vm_ month      _fh_ by top headline     _hA_ archive       _tA_ archive files
_vy_ year       _fx_ by regexp           _h:_ set tags      _tr_ clock report
_vn_ next span  _fd_ delete all filters  _hp_ set priority  _td_ diaries
_vp_ prev span  ^^                       ^^                 ^^
_vr_ reset      ^^                       ^^                 ^^
^^              ^^                       ^^                 ^^
"
  ;; Entry
  ("hA" org-agenda-archive-default)
  ("hk" org-agenda-kill)
  ("hp" org-agenda-priority)
  ("hr" org-agenda-refile)
  ("h:" org-agenda-set-tags)
  ("ht" org-agenda-todo)
  ;; Visit entry
  ("o"   link-hint-open-link :exit t)
  ("<tab>" org-agenda-goto :exit t)
  ("TAB" org-agenda-goto :exit t)
  ("SPC" org-agenda-show-and-scroll-up)
  ("RET" org-agenda-switch-to :exit t)
  ;; Date
  ("dt" org-agenda-date-prompt)
  ("dd" org-agenda-deadline)
  ("+" org-agenda-do-date-later)
  ("-" org-agenda-do-date-earlier)
  ("ds" org-agenda-schedule)
  ;; View
  ("vd" org-agenda-day-view)
  ("vw" org-agenda-week-view)
  ("vt" org-agenda-fortnight-view)
  ("vm" org-agenda-month-view)
  ("vy" org-agenda-year-view)
  ("vn" org-agenda-later)
  ("vp" org-agenda-earlier)
  ("vr" org-agenda-reset-view)
  ;; Toggle mode
  ("ta" org-agenda-archives-mode)
  ("tA" (org-agenda-archives-mode 'files))
  ("tr" org-agenda-clockreport-mode)
  ("tf" org-agenda-follow-mode)
  ("tl" org-agenda-log-mode)
  ("td" org-agenda-toggle-diary)
  ;; Filter
  ("fc" org-agenda-filter-by-category)
  ("fx" org-agenda-filter-by-regexp)
  ("ft" org-agenda-filter-by-tag)
  ("fr" org-agenda-filter-by-tag-refine)
  ("fh" org-agenda-filter-by-top-headline)
  ("fd" org-agenda-filter-remove-all)
  ;; Clock
  ("cq" org-agenda-clock-cancel)
  ("cj" org-agenda-clock-goto :exit t)
  ("ci" org-agenda-clock-in :exit t)
  ("co" org-agenda-clock-out)
  ;; Other
  ("q" nil :exit t)
  ("gd" org-agenda-goto-date)
  ("." org-agenda-goto-today)
  ("gr" org-agenda-redo))


;; ======== EMACS SLACK ========
(use-package slack
  :commands (slack-start)
  :init
  (setq slack-buffer-emojify t) ;; if you want to enable emoji, default nil
  (setq slack-prefer-current-team t)
  :config
  (slack-register-team
   :name "emacs-slack"
   :default t
   :client-id "3589744462.474184672579"
   :client-secret "54ecac9b80719fad28134d0b5264b254"
   :token "xoxs-3589744462-405502340496-473458263857-93503c84e2abd0ce9118541f7a0a0d9adb8f821606f2a714ea5b1ed3e4960bf5"
   :subscribed-channels '(cases lunch pong)
   :full-and-display-names t)

  ;; (slack-register-team
  ;;  :name "test"
  ;;  :client-id "3333333333.77777777777"
  ;;  :client-secret "cccccccccccccccccccccccccccccccc"
  ;;  :token "xoxs-yyyyyyyyyy-zzzzzzzzzzz-hhhhhhhhhhh-llllllllll"
  ;;  :subscribed-channels '(hoge fuga))

  (evil-define-key 'normal slack-info-mode-map
	",u" 'slack-room-update-messages)
  (evil-define-key 'normal slack-mode-map
	",c" 'slack-buffer-kill
	",ra" 'slack-message-add-reaction
	",rr" 'slack-message-remove-reaction
	",rs" 'slack-message-show-reaction-users
	",pl" 'slack-room-pins-list
	",pa" 'slack-message-pins-add
	",pr" 'slack-message-pins-remove
	",mm" 'slack-message-write-another-buffer
	",me" 'slack-message-edit
	",md" 'slack-message-delete
	",u" 'slack-room-update-messages
	",2" 'slack-message-embed-mention
	",3" 'slack-message-embed-channel
	"\C-n" 'slack-buffer-goto-next-message
	"\C-p" 'slack-buffer-goto-prev-message)
  (evil-define-key 'normal slack-edit-message-mode-map
	",k" 'slack-message-cancel-edit
	",s" 'slack-message-send-from-buffer
	",2" 'slack-message-embed-mention
	",3" 'slack-message-embed-channel))

(use-package alert
  :commands (alert)
  :init
  (setq alert-default-style 'notifier))


;; ======== EDITOR MACROS ========
(defhydra hydra-macro (:hint nil :color pink :pre 
							 (when defining-kbd-macro
							   (kmacro-end-macro 1)))
  "
  ^Create-Cycle^   ^Basic^           ^Insert^        ^Save^         ^Edit^
╭─────────────────────────────────────────────────────────────────────────╯
	 ^_i_^           [_e_] execute    [_n_] insert    [_b_] name      [_'_] previous
	 ^^↑^^           [_d_] delete     [_t_] set       [_K_] key       [_,_] last
 _j_ ←   → _l_       [_o_] edit       [_a_] add       [_x_] register     
	 ^^↓^^           [_r_] region     [_f_] format    [_B_] defun
	 ^_k_^           [_m_] step
	^^   ^^          [_s_] swap
"
  ("j" kmacro-start-macro :color blue)
  ("l" kmacro-end-or-call-macro-repeat)
  ("i" kmacro-cycle-ring-previous)
  ("k" kmacro-cycle-ring-next)
  ("r" apply-macro-to-region-lines)
  ("d" kmacro-delete-ring-head)
  ("e" kmacro-end-or-call-macro-repeat)
  ("o" kmacro-edit-macro-repeat)
  ("m" kmacro-step-edit-macro)
  ("s" kmacro-swap-ring)
  ("n" kmacro-insert-counter)
  ("t" kmacro-set-counter)
  ("a" kmacro-add-counter)
  ("f" kmacro-set-format)
  ("b" kmacro-name-last-macro)
  ("K" kmacro-bind-to-key)
  ("B" insert-kbd-macro)
  ("x" kmacro-to-register)
  ("'" kmacro-edit-macro)
  ("," edit-kbd-macro)
  ("q" nil :color blue))


;; ======== CUSTOM MINI-MODULES ========
(defun copy-buffer-to-clipboard ()
  "Copies current buffer contents to system clipboard."
  (interactive)
  (clipboard-kill-ring-save (point-min) (point-max)))


;; ======== HELPER FUNCTIONS ======== 
(defun open-config-file()
  (interactive)
  (find-file "~/.emacs.d/init.el"))

(defun reload-config-file()
  (interactive)
  (load-file "~/.emacs.d/init.el"))

(defun new-eshell ()
  "Create a new instance of eshell in a window split."
  (interactive)
  (let* ((lines (window-body-height))
		 (new-window (split-window-vertically (floor (* 0.7 lines)))))
	(select-window new-window)
	(eshell "eshell"))) 

(defun comint-delchar-or-eof-or-kill-buffer (arg)
  "C-d on empty line in shell kills process. C-d again kills shell."
  (interactive "p")
  (if (null (get-buffer-process (current-buffer)))
	  (kill-buffer)
	(comint-delchar-or-maybe-eof arg)))

(add-hook 'shell-mode-hook
		  (lambda ()
			(define-key shell-mode-map
			  (kbd "C-d") 'comint-delchar-or-eof-or-kill-buffer)))

(defun kill-buffer-and-window ()
  "Kill buffer and window."
  (interactive)
  (kill-this-buffer)
  (evil-window-delete))

(defun enable-paredit-nonlisp ()
  "Turn on paredit mode for non-lisps."
  (interactive)
  (set (make-local-variable 'paredit-space-for-delimiter-predicates)
	   '((lambda (endp delimiter) nil)))
  (paredit-mode 1)) 
(sp-local-pair 'c++-mode "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))

(defun switch-to-previous-buffer ()
  "Switch to previously open buffer. Repeated invocations toggle between two most recently opened buffers."
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))

(defun my-create-newline-and-enter-sexp (&rest _ignored)
  "Open a new brace or bracket expression, with relevant newlines and indent."
  (newline)
  (indent-according-to-mode)
  (forward-line -1)
  (indent-according-to-mode))

(defun sp-paredit-like-close-round ()
  "If the next character is a closing character as according to smartparens skip it, otherwise insert `last-input-event'."
  (interactive)
  (let ((pt (point)))
	(if (and (< pt (point-max))
			 (sp--char-is-part-of-closing (buffer-substring-no-properties pt (1+ pt))))
		(forward-char 1)
	  (call-interactively #'self-insert-command))))

(defun hydra-by-major-mode ()
  "Selects specific hydra to be used based on major mode."
  (interactive)
  (cl-case major-mode
	;; major modes with their associated hydras
	(emacs-lisp-mode
	 (major-mode-hydra))
  ;; MERGE - old setting, maybe try it sometimes
  ;; (emacs-lisp-mode
   ;; (hydra-elisp/body)) 
	(haskell-mode
	 (hydra-haskell/body))
	(haskell-cabal-mode
	 (hydra-haskell-cabal/body))
	(c-mode
	 (hydra-lsp/body))
	(js2-mode
	 (hydra-js-lsp/body))
	(typescript-mode
	 (hydra-js-lsp/body))
	(rjsx-mode
	 (hydra-react/body))
	(php-mode
	 (hydra-lsp/body))
	(web-mode
	 (hydra-lsp/body))
	(css-mode
	 (hydra-lsp/body))
	(markdown-mode
	 (hydra-markdown/body))
	(org-mode
	 (hydra-org-mode/body))
	(org-agenda-mode
	 (hydra-org-agenda/body))
	(lisp-mode
	 (hydra-slime-mode/body))
	(package-menu-mode
	 (hydra-package-menu/body))
	(t
	 (error "%S not supported" major-mode))))
(global-set-key (kbd "C-c m") 'hydra-by-major-mode)

(defun company-mode/backend-with-yas (backend)
  "Integrate yas suggestions into company completion window."
  (if (or (not company-mode/enable-yas) (and (listp backend) (member 'company-yasnippet backend)))
	  backend
	(append (if (consp backend) backend (list backend))
			'(:with company-yasnippet))))
;; call function to add to company backends
(setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))

(defun save-all-buffers ()
  "Save all open buffers."
  (interactive)
  (save-some-buffers t))

(defun collapse-next-line ()
  "Pulls next line up to current."
  (interactive)
  (join-line -1))

(defun copy-file-name-to-clipboard ()
  "Copy the current buffer file name to the clipboard."
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
            default-directory
          (buffer-file-name))))
  (when filename
    (kill-new filename)
    (message "Copied buffer file name '%s' to the clipboard." filename))))

(defun xah-toggle-margin-right ()
  "Toggle the right margin between `fill-column' or window width.
This command is convenient when reading novel, documentation."
  (interactive)
  (if (eq (cdr (window-margins)) nil)
	  (set-window-margins nil 0 (- (window-body-width) fill-column))
	(set-window-margins nil 0 0)))

(defun rotate-windows ()
  "Switch buffers in two windows, preserving layout."
  (interactive)
  (cond ((not (> (count-windows)1))
     (message "You can't rotate a single window!"))
    (t
     (setq i 1)
     (setq numWindows (count-windows))
     (while  (< i numWindows)
       (let* (
          (w1 (elt (window-list) i))
          (w2 (elt (window-list) (+ (% i numWindows) 1)))

          (b1 (window-buffer w1))
          (b2 (window-buffer w2))

          (s1 (window-start w1))
          (s2 (window-start w2))
          )
       (set-window-buffer w1  b2)
       (set-window-buffer w2 b1)
       (set-window-start w1 s2)
       (set-window-start w2 s1)
       (setq i (1+ i)))))))

(defun delete-current-buffer-file ()
  "Remove file connected to current buffer and kill buffer."
  (interactive)
  (let ((filename (buffer-file-name))
    (buffer (current-buffer))
    (name (buffer-name)))
  (if (not (and filename (file-exists-p filename)))
    (ido-kill-buffer)
    (when (yes-or-no-p "Are you sure you want to remove this file? ")
    (delete-file filename)
    (kill-buffer buffer)
    (message "File '%s' successfully removed" filename)))))

(defun rename-current-buffer-file ()
  "Renames current buffer and file it is visiting."
  (interactive)
  (let ((name (buffer-name))
    (filename (buffer-file-name)))
  (if (not (and filename (file-exists-p filename)))
    (error "Buffer '%s' is not visiting a file!" name)
    (let ((new-name (read-file-name "New name: " filename)))
    (if (get-buffer new-name)
      (error "A buffer named '%s' already exists!" new-name)
      (rename-file filename new-name 1)
      (rename-buffer new-name)
      (set-visited-file-name new-name)
      (set-buffer-modified-p nil)
      (message "File '%s' successfully renamed to '%s'"
           name (file-name-nondirectory new-name)))))))

(defun move-line-down ()
  (interactive)
  (let ((col (current-column)))
  (save-excursion
    (forward-line)
    (transpose-lines 1))
  (forward-line)
  (move-to-column col)))

(defun move-line-up ()
  (interactive)
  (let ((col (current-column)))
  (save-excursion
    (forward-line)
    (transpose-lines -1))
  (move-to-column col)))

(defun display-entire-org-file ()
  "Shows the whole org file, including drawers."
  (universal-argument)
  (universal-argument)
  (universal-argument)
  (org-cycle))

(defun my/describe-random-interactive-function ()
  (interactive)
  "Show the documentation for a random interactive function.
Consider only documented, non-obsolete functions."
  (let (result)
  (mapatoms
   (lambda (s)
     (when (and (commandp s) 
          (documentation s t)
          (null (get s 'byte-obsolete-info)))
     (setq result (cons s result)))))
  (describe-function (elt result (random (length result))))))



;; CONFIG SAMPLES TO LEARN FROM

;; use-package example for single mode
;; (use-package markdown-mode
;;   :mode (("README\\.md\\'" . gfm-mode)
;;          ("\\.md\\'" . markdown-mode)
;;          ("\\.markdown\\'" . markdown-mode))
;;   :init (setq markdown-command "multimarkdown")
;;   :general (
;;     :states '(normal)
;;     :prefix "SPC"
;;     "m"    '(:ignore t :which-key "markdown")
;;     "ms"   '(:keymap markdown-mode-style-map :which-key "style")
;;     "mc"   '(:keymap markdown-mode-command-map :which-key "command")
;;     )
;;   )


;; ----------------------------------------------------------------------------------------
;;              END OF CONFIG - BEGINNING OF CUSTOM - DO NOT EDIT PAST HERE
;; ----------------------------------------------------------------------------------------


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(evil-collection-minibuffer-setup nil t)
 '(package-selected-packages
   (quote
	(js-comint yasnippet-snippets xref-js2 which-key web-mode treemacs-projectile treemacs-magit treemacs-icons-dired treemacs-evil tide sws-mode suggest sublimity solarized-theme slime-company slack shut-up scribble-mode rjsx-mode restclient rainbow-delimiters racket-mode quelpa-use-package powerline php-auto-yasnippets org-trello org-plus-contrib org-jira org-bullets nodejs-repl morlock magit-popup lsp-ui lsp-treemacs lisp-extra-font-lock key-chord json-mode js2-highlight-vars js-doc jade-mode ivy-xref ivy-hydra indium hindent highlight-operators highlight-numbers highlight-indent-guides highlight-function-calls highlight-escape-sequences highlight-doxygen highlight-defined highlight-blocks helpful helm-lsp gruvbox-theme graphql ghub general geben gdb-mi eyebrowse evil-surround evil-smartparens evil-magit evil-collection evil-cleverparens eldoc-eval editorconfig dumb-jump doom-themes doom-modeline cquery counsel-projectile corral company-web company-quickhelp company-php company-lsp company-ghc company-box color-theme-sanityinc-solarized color-theme cl-lib-highlight auto-org-md add-node-modules-path))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

